FROM openjdk:11-jdk-slim

WORKDIR /src
COPY . /src
RUN apt-get update && apt-get install -y curl
WORKDIR /run
RUN cp /src/build/libs/*.jar /run/server.jar
EXPOSE 80
CMD ["sh", "-c", "java -server -XX:+UnlockExperimentalVMOptions -XX:InitialRAMFraction=2 -XX:MinRAMFraction=2 -XX:MaxRAMFraction=2 -XX:+UseG1GC -XX:MaxGCPauseMillis=100 -XX:+UseStringDeduplication -jar server.jar"]
HEALTHCHECK --interval=60s --timeout=5s --retries=2 CMD curl --fail http://0.0.0.0:80/health || kill 7
