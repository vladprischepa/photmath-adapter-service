{
  "result": {
    "header": "Solve for $m$",
    "solution": "\\begin{align*}&\\begin{array} { l }m_1=\\frac{ -1-\\sqrt{ 10 } }{ 3 },& m_2=\\frac{ -1+\\sqrt{ 10 } }{ 3 }\\end{array} \\\\&\\begin{array} { l }m_1\\approx-1.38743,& m_2\\approx0.720759\\end{array}\\end{align*}",
    "steps": [
      {
        "headers": [
          "Determine the defined range "
        ],
        "substeps": [
          {
            "description": "Determine the defined range ",
            "left": "\\frac{ 1 }{ 3 }=m+\\frac{ m-1 }{ m }",
            "right": "\\begin{array} { l }\\frac{ 1 }{ 3 }=m+\\frac{ m-1 }{ m },& m≠0\\end{array}",
            "subresult": {
              "header": "Determine the defined range ",
              "solution": "\\begin{array} { l }\\frac{ 1 }{ 3 }=m+\\frac{ m-1 }{ m },& m≠0\\end{array}",
              "steps": [
                {
                  "headers": [
                    "Find the restricted values"
                  ],
                  "substeps": [
                    {
                      "description": "Find all values of $m$ that make the denominator of $\\frac{ m-1 }{ m }$ equal to $0$ ",
                      "left": "\\frac{ 1 }{ 3 }=m+\\frac{ m-1 }{ m }",
                      "right": "m=0"
                    }
                  ]
                },
                {
                  "headers": [
                    "Exclude the restricted values"
                  ],
                  "substeps": [
                    {
                      "description": "To find the defined range, exclude the restricted values ",
                      "left": "m=0",
                      "right": "\\begin{array} { l }\\frac{ 1 }{ 3 }=m+\\frac{ m-1 }{ m },& m≠0\\end{array}"
                    }
                  ]
                }
              ]
            }
          }
        ]
      },
      {
        "headers": [
          "Move the expression to the left"
        ],
        "substeps": [
          {
            "description": "Move the expression to the left-hand side and change its sign ",
            "left": "\\begin{array} { l }\\frac{ 1 }{ 3 }=m+\\frac{ m-1 }{ m },& m≠0\\end{array}",
            "right": "\\frac{ 1 }{ 3 }-m-\\frac{ m-1 }{ m }=0",
            "subresult": {
              "header": "Move the expression to the left",
              "solution": "\\frac{ 1 }{ 3 }-m-\\frac{ m-1 }{ m }=0",
              "steps": [
                {
                  "headers": [
                    "Move the expression to the left"
                  ],
                  "substeps": [
                    {
                      "description": "Move the expression to the left-hand side by adding its opposite to both sides ",
                      "left": "\\frac{ 1 }{ 3 }=m+\\frac{ m-1 }{ m }",
                      "right": "\\frac{ 1 }{ 3 }-m-\\frac{ m-1 }{ m }=m+\\frac{ m-1 }{ m }-m-\\frac{ m-1 }{ m }"
                    }
                  ]
                },
                {
                  "headers": [
                    "Eliminate the opposites"
                  ],
                  "substeps": [
                    {
                      "description": "The sum of two opposites equals $0$ ",
                      "left": "\\frac{ 1 }{ 3 }-m-\\frac{ m-1 }{ m }=m+\\frac{ m-1 }{ m }-m-\\frac{ m-1 }{ m }",
                      "right": "\\frac{ 1 }{ 3 }-m-\\frac{ m-1 }{ m }=0"
                    }
                  ]
                }
              ]
            }
          }
        ]
      },
      {
        "headers": [
          "Transform the expression"
        ],
        "substeps": [
          {
            "description": "Write all numerators above the least common denominator $3m$",
            "left": "\\frac{ 1 }{ 3 }-m-\\frac{ m-1 }{ m }=0",
            "right": "\\frac{ m-3{m}^{2}-3\\left( m-1 \\right) }{ 3m }=0",
            "subresult": {
              "header": "Transform the expression",
              "solution": "\\frac{ m-3{m}^{2}-3\\left( m-1 \\right) }{ 3m }",
              "steps": [
                {
                  "headers": [
                    "Expand",
                    "Convert"
                  ],
                  "substeps": [
                    {
                      "description": "Expand the fraction to get the least common denominator ",
                      "left": "\\frac{ 1 }{ 3 }-m-\\frac{ m-1 }{ m }",
                      "right": "\\frac{ m \\times 1 }{ m \\times 3 }-m-\\frac{ m-1 }{ m }"
                    },
                    {
                      "description": "Use $a=\\frac{ a }{ 1 }$ to convert the expression into a fraction ",
                      "left": "\\frac{ m \\times 1 }{ m \\times 3 }-m-\\frac{ m-1 }{ m }",
                      "right": "\\frac{ m \\times 1 }{ m \\times 3 }-\\frac{ m }{ 1 }-\\frac{ m-1 }{ m }"
                    },
                    {
                      "description": "Expand the fraction to get the least common denominator ",
                      "left": "\\frac{ m \\times 1 }{ m \\times 3 }-\\frac{ m }{ 1 }-\\frac{ m-1 }{ m }",
                      "right": "\\frac{ m \\times 1 }{ m \\times 3 }-\\frac{ m }{ 1 }-\\frac{ 3\\left( m-1 \\right) }{ 3m }"
                    }
                  ]
                },
                {
                  "headers": [
                    "Multiply",
                    "Reorder",
                    "Expand"
                  ],
                  "substeps": [
                    {
                      "description": "Any expression multiplied by $1$ remains the same",
                      "left": "\\frac{ m \\times 1 }{ m \\times 3 }-\\frac{ m }{ 1 }-\\frac{ 3\\left( m-1 \\right) }{ 3m }",
                      "right": "\\frac{ m }{ m \\times 3 }-\\frac{ m }{ 1 }-\\frac{ 3\\left( m-1 \\right) }{ 3m }"
                    },
                    {
                      "description": "Use the commutative property to reorder the terms",
                      "left": "\\frac{ m }{ m \\times 3 }-\\frac{ m }{ 1 }-\\frac{ 3\\left( m-1 \\right) }{ 3m }",
                      "right": "\\frac{ m }{ 3m }-\\frac{ m }{ 1 }-\\frac{ 3\\left( m-1 \\right) }{ 3m }"
                    },
                    {
                      "description": "Expand the fraction to get the least common denominator ",
                      "left": "\\frac{ m }{ 3m }-\\frac{ m }{ 1 }-\\frac{ 3\\left( m-1 \\right) }{ 3m }",
                      "right": "\\frac{ m }{ 3m }-\\frac{ 3m \\times m }{ 3m \\times 1 }-\\frac{ 3\\left( m-1 \\right) }{ 3m }"
                    }
                  ]
                },
                {
                  "headers": [
                    "Calculate",
                    "Multiply"
                  ],
                  "substeps": [
                    {
                      "description": "Calculate the product",
                      "left": "\\frac{ m }{ 3m }-\\frac{ 3m \\times m }{ 3m \\times 1 }-\\frac{ 3\\left( m-1 \\right) }{ 3m }",
                      "right": "\\frac{ m }{ 3m }-\\frac{ 3{m}^{2} }{ 3m \\times 1 }-\\frac{ 3\\left( m-1 \\right) }{ 3m }"
                    },
                    {
                      "description": "Any expression multiplied by $1$ remains the same",
                      "left": "\\frac{ m }{ 3m }-\\frac{ 3{m}^{2} }{ 3m \\times 1 }-\\frac{ 3\\left( m-1 \\right) }{ 3m }",
                      "right": "\\frac{ m }{ 3m }-\\frac{ 3{m}^{2} }{ 3m }-\\frac{ 3\\left( m-1 \\right) }{ 3m }"
                    }
                  ]
                },
                {
                  "headers": [
                    "Transform the expression"
                  ],
                  "substeps": [
                    {
                      "description": "Write all numerators above the common denominator",
                      "left": "\\frac{ m }{ 3m }-\\frac{ 3{m}^{2} }{ 3m }-\\frac{ 3\\left( m-1 \\right) }{ 3m }",
                      "right": "\\frac{ m-3{m}^{2}-3\\left( m-1 \\right) }{ 3m }"
                    }
                  ]
                }
              ]
            }
          }
        ]
      },
      {
        "headers": [
          "Remove the parentheses"
        ],
        "substeps": [
          {
            "description": "Distribute $-3$ through the parentheses",
            "left": "\\frac{ m-3{m}^{2}-3\\left( m-1 \\right) }{ 3m }=0",
            "right": "\\frac{ m-3{m}^{2}-3m+3 }{ 3m }=0",
            "subresult": {
              "header": "Remove the parentheses",
              "solution": "-3m+3",
              "steps": [
                {
                  "headers": [
                    "Distribute $-3$"
                  ],
                  "substeps": [
                    {
                      "description": "Multiply each term in the parentheses by $-3$",
                      "left": "-3\\left( m-1 \\right)",
                      "right": "-3m-3 \\times \\left( -1 \\right)"
                    }
                  ]
                },
                {
                  "headers": [
                    "Calculate the product"
                  ],
                  "substeps": [
                    {
                      "description": "Any expression multiplied by $-1$ equals its opposite",
                      "left": "-3m-3 \\times \\left( -1 \\right)",
                      "right": "-3m+3"
                    }
                  ]
                }
              ]
            }
          }
        ]
      },
      {
        "headers": [
          "Collect like terms"
        ],
        "substeps": [
          {
            "description": "Collect like terms",
            "left": "\\frac{ m-3{m}^{2}-3m+3 }{ 3m }=0",
            "right": "\\frac{ -2m-3{m}^{2}+3 }{ 3m }=0",
            "subresult": {
              "header": "Collect like terms",
              "solution": "-2m",
              "steps": [
                {
                  "headers": [
                    "The coefficient is $1$"
                  ],
                  "substeps": [
                    {
                      "description": "If a term doesn't have a coefficient, it is considered that the coefficient is $1$ ",
                      "left": "m-3m",
                      "right": "1m-3m"
                    }
                  ]
                },
                {
                  "headers": [
                    "Collect like terms"
                  ],
                  "substeps": [
                    {
                      "description": "Collect like terms by subtracting their coefficients",
                      "left": "1m-3m",
                      "right": "\\left( 1-3 \\right)m"
                    }
                  ]
                },
                {
                  "headers": [
                    "Calculate"
                  ],
                  "substeps": [
                    {
                      "description": "Calculate the difference",
                      "left": "\\left( 1-3 \\right)m",
                      "right": "-2m"
                    }
                  ]
                }
              ]
            }
          }
        ]
      },
      {
        "headers": [
          "Set the numerator equal to $0$"
        ],
        "substeps": [
          {
            "description": "When the quotient of expressions equals $0$, the numerator has to be $0$ ",
            "left": "\\frac{ -2m-3{m}^{2}+3 }{ 3m }=0",
            "right": "-2m-3{m}^{2}+3=0"
          }
        ]
      },
      {
        "headers": [
          "Reorder the terms"
        ],
        "substeps": [
          {
            "description": "Use the commutative property to reorder the terms",
            "left": "-2m-3{m}^{2}+3=0",
            "right": "-3{m}^{2}-2m+3=0"
          }
        ]
      },
      {
        "headers": [
          "Change the signs"
        ],
        "substeps": [
          {
            "description": "Change the signs on both sides of the equation ",
            "left": "-3{m}^{2}-2m+3=0",
            "right": "3{m}^{2}+2m-3=0",
            "subresult": {
              "header": "Change the signs",
              "solution": "3{m}^{2}+2m-3=0",
              "steps": [
                {
                  "headers": [
                    "Multiply both sides"
                  ],
                  "substeps": [
                    {
                      "description": "Multiply both sides of the equation by $-1$",
                      "left": "-3{m}^{2}-2m+3=0",
                      "right": "-1 \\times \\left( -3 \\right){m}^{2}-1 \\times \\left( -2 \\right)m-1 \\times 3=-1 \\times 0"
                    }
                  ]
                },
                {
                  "headers": [
                    "Multiply"
                  ],
                  "substeps": [
                    {
                      "description": "Multiplying an even number of negative terms makes the product positive ",
                      "left": "-1 \\times \\left( -3 \\right){m}^{2}-1 \\times \\left( -2 \\right)m-1 \\times 3=-1 \\times 0",
                      "right": "1 \\times 3{m}^{2}-1 \\times \\left( -2 \\right)m-1 \\times 3=-1 \\times 0"
                    },
                    {
                      "description": "Any expression multiplied by $1$ remains the same",
                      "left": "1 \\times 3{m}^{2}-1 \\times \\left( -2 \\right)m-1 \\times 3=-1 \\times 0",
                      "right": "3{m}^{2}-1 \\times \\left( -2 \\right)m-1 \\times 3=-1 \\times 0"
                    },
                    {
                      "description": "Multiplying an even number of negative terms makes the product positive ",
                      "left": "3{m}^{2}-1 \\times \\left( -2 \\right)m-1 \\times 3=-1 \\times 0",
                      "right": "3{m}^{2}+1 \\times 2m-1 \\times 3=-1 \\times 0"
                    },
                    {
                      "description": "Any expression multiplied by $1$ remains the same",
                      "left": "3{m}^{2}+1 \\times 2m-1 \\times 3=-1 \\times 0",
                      "right": "3{m}^{2}+2m-1 \\times 3=-1 \\times 0"
                    },
                    {
                      "description": "Any expression multiplied by $1$ remains the same",
                      "left": "3{m}^{2}+2m-1 \\times 3=-1 \\times 0",
                      "right": "3{m}^{2}+2m-3=-1 \\times 0"
                    },
                    {
                      "description": "Any expression multiplied by $0$ equals $0$",
                      "left": "3{m}^{2}+2m-3=-1 \\times 0",
                      "right": "3{m}^{2}+2m-3=0"
                    }
                  ]
                }
              ]
            }
          }
        ]
      },
      {
        "headers": [
          "Identify the coefficients"
        ],
        "substeps": [
          {
            "description": "Identify the coefficients $a$, $b$ and $c$ of the quadratic equation ",
            "left": "3{m}^{2}+2m-3=0",
            "right": "\\begin{array} { l }a=3,& b=2,& c=-3\\end{array}",
            "subresult": {
              "header": "Identify the coefficients",
              "solution": "\\begin{array} { l }a=3,& b=2,& c=-3\\end{array}",
              "steps": [
                {
                  "headers": [
                    "Transform the expression"
                  ],
                  "substeps": [
                    {
                      "description": "Subtracting is the same as adding the opposite",
                      "left": "3{m}^{2}+2m-3=0",
                      "right": "3{m}^{2}+2m+\\left( -3 \\right)=0"
                    }
                  ]
                },
                {
                  "headers": [
                    "Identify the coefficients"
                  ],
                  "substeps": [
                    {
                      "description": "Identify the coefficients $a$, $b$ and $c$ of the quadratic equation ",
                      "left": "3{m}^{2}+2m+\\left( -3 \\right)=0",
                      "right": "\\begin{array} { l }a=3,& b=2,& c=-3\\end{array}"
                    }
                  ]
                }
              ]
            }
          }
        ]
      },
      {
        "headers": [
          "Use the quadratic formula"
        ],
        "substeps": [
          {
            "description": "Substitute $a=3$, $b=2$ and $c=-3$ into the quadratic formula $m=\\frac{ -b\\pm\\sqrt{ {b}^{2}-4ac } }{ 2a }$",
            "left": "\\begin{array} { l }a=3,& b=2,& c=-3\\end{array}",
            "right": "m=\\frac{ -2\\pm\\sqrt{ {2}^{2}-4 \\times 3 \\times \\left( -3 \\right) } }{ 2 \\times 3 }"
          }
        ]
      },
      {
        "headers": [
          "Evaluate",
          "Calculate"
        ],
        "substeps": [
          {
            "description": "Evaluate the power",
            "left": "m=\\frac{ -2\\pm\\sqrt{ {2}^{2}-4 \\times 3 \\times \\left( -3 \\right) } }{ 2 \\times 3 }",
            "right": "m=\\frac{ -2\\pm\\sqrt{ 4-4 \\times 3 \\times \\left( -3 \\right) } }{ 2 \\times 3 }",
            "subresult": {
              "header": "Evaluate",
              "solution": "4",
              "steps": [
                {
                  "headers": [
                    "Write as a multiplication"
                  ],
                  "substeps": [
                    {
                      "description": "Write the exponentiation as a multiplication",
                      "left": "{2}^{2}",
                      "right": "2 \\times 2"
                    }
                  ]
                },
                {
                  "headers": [
                    "Multiply"
                  ],
                  "substeps": [
                    {
                      "description": "Multiply the numbers",
                      "left": "2 \\times 2",
                      "right": "4"
                    }
                  ]
                }
              ]
            }
          },
          {
            "description": "Calculate the product",
            "left": "m=\\frac{ -2\\pm\\sqrt{ 4-4 \\times 3 \\times \\left( -3 \\right) } }{ 2 \\times 3 }",
            "right": "m=\\frac{ -2\\pm\\sqrt{ 4+36 } }{ 2 \\times 3 }",
            "subresult": {
              "header": "Calculate",
              "solution": "36",
              "steps": [
                {
                  "headers": [
                    "Multiply"
                  ],
                  "substeps": [
                    {
                      "description": "Multiplying an even number of negative terms makes the product positive ",
                      "left": "-4 \\times 3 \\times \\left( -3 \\right)",
                      "right": "4 \\times 3 \\times 3"
                    },
                    {
                      "description": "Multiply the numbers",
                      "left": "4 \\times 3 \\times 3",
                      "right": "12 \\times 3"
                    }
                  ]
                },
                {
                  "headers": [
                    "Multiply"
                  ],
                  "substeps": [
                    {
                      "description": "Multiply the numbers",
                      "left": "12 \\times 3",
                      "right": "36"
                    }
                  ]
                }
              ]
            }
          },
          {
            "description": "Multiply the numbers",
            "left": "m=\\frac{ -2\\pm\\sqrt{ 4+36 } }{ 2 \\times 3 }",
            "right": "m=\\frac{ -2\\pm\\sqrt{ 4+36 } }{ 6 }"
          }
        ]
      },
      {
        "headers": [
          "Calculate"
        ],
        "substeps": [
          {
            "description": "Add the numbers",
            "left": "m=\\frac{ -2\\pm\\sqrt{ 4+36 } }{ 6 }",
            "right": "m=\\frac{ -2\\pm\\sqrt{ 40 } }{ 6 }"
          }
        ]
      },
      {
        "headers": [
          "Simplify the expression"
        ],
        "substeps": [
          {
            "description": "Simplify the radical expression ",
            "left": "m=\\frac{ -2\\pm\\sqrt{ 40 } }{ 6 }",
            "right": "m=\\frac{ -2\\pm2\\sqrt{ 10 } }{ 6 }",
            "subresult": {
              "header": "Simplify the expression",
              "solution": "2\\sqrt{ 10 }",
              "steps": [
                {
                  "headers": [
                    "Write as a product"
                  ],
                  "substeps": [
                    {
                      "description": "Write the expression as a product where the root of one of the factors can be evaluated",
                      "left": "\\sqrt{ 40 }",
                      "right": "\\sqrt{ 4 \\times 10 }"
                    }
                  ]
                },
                {
                  "headers": [
                    "Write in exponential form"
                  ],
                  "substeps": [
                    {
                      "description": "Write the expression in exponential form with the base of $2$",
                      "left": "\\sqrt{ 4 \\times 10 }",
                      "right": "\\sqrt{ {2}^{2} \\times 10 }"
                    }
                  ]
                },
                {
                  "headers": [
                    "Use the properties of radicals"
                  ],
                  "substeps": [
                    {
                      "description": "The root of a product is equal to the product of the roots of each factor",
                      "left": "\\sqrt{ {2}^{2} \\times 10 }",
                      "right": "\\sqrt{ {2}^{2} }\\sqrt{ 10 }"
                    }
                  ]
                },
                {
                  "headers": [
                    "Simplify the root"
                  ],
                  "substeps": [
                    {
                      "description": "Reduce the index of the radical and exponent with $2$",
                      "left": "\\sqrt{ {2}^{2} }\\sqrt{ 10 }",
                      "right": "2\\sqrt{ 10 }"
                    }
                  ]
                }
              ]
            }
          }
        ]
      },
      {
        "headers": [
          "Separate the solutions"
        ],
        "substeps": [
          {
            "description": "Write the solutions, one with a $+$ sign and one with a $-$ sign",
            "left": "m=\\frac{ -2\\pm2\\sqrt{ 10 } }{ 6 }",
            "right": "\\begin{array} { l }m=\\frac{ -2+2\\sqrt{ 10 } }{ 6 },\\\\m=\\frac{ -2-2\\sqrt{ 10 } }{ 6 }\\end{array}"
          }
        ]
      },
      {
        "headers": [
          "Simplify"
        ],
        "substeps": [
          {
            "description": "Simplify the expression",
            "left": "\\begin{array} { l }m=\\frac{ -2+2\\sqrt{ 10 } }{ 6 },\\\\m=\\frac{ -2-2\\sqrt{ 10 } }{ 6 }\\end{array}",
            "right": "\\begin{array} { l }m=\\frac{ -1+\\sqrt{ 10 } }{ 3 },\\\\m=\\frac{ -2-2\\sqrt{ 10 } }{ 6 }\\end{array}",
            "subresult": {
              "header": "Simplify",
              "solution": "\\frac{ -1+\\sqrt{ 10 } }{ 3 }",
              "steps": [
                {
                  "headers": [
                    "Factor the expression"
                  ],
                  "substeps": [
                    {
                      "description": "Factor out $2$ from the expression",
                      "left": "\\frac{ -2+2\\sqrt{ 10 } }{ 6 }",
                      "right": "\\frac{ 2\\left( -1+\\sqrt{ 10 } \\right) }{ 6 }"
                    }
                  ]
                },
                {
                  "headers": [
                    "Reduce the fraction"
                  ],
                  "substeps": [
                    {
                      "description": "Cancel out the common factor $2$ ",
                      "left": "\\frac{ 2\\left( -1+\\sqrt{ 10 } \\right) }{ 6 }",
                      "right": "\\frac{ -1+\\sqrt{ 10 } }{ 3 }"
                    }
                  ]
                }
              ]
            }
          },
          {
            "description": "Simplify the expression",
            "left": "\\begin{array} { l }m=\\frac{ -1+\\sqrt{ 10 } }{ 3 },\\\\m=\\frac{ -2-2\\sqrt{ 10 } }{ 6 }\\end{array}",
            "right": "\\begin{array} { l }\\begin{array} { l }m=\\frac{ -1+\\sqrt{ 10 } }{ 3 },\\\\m=\\frac{ -1-\\sqrt{ 10 } }{ 3 }\\end{array},& m≠0\\end{array}",
            "subresult": {
              "header": "Simplify",
              "solution": "\\frac{ -1-\\sqrt{ 10 } }{ 3 }",
              "steps": [
                {
                  "headers": [
                    "Factor the expression"
                  ],
                  "substeps": [
                    {
                      "description": "Factor out $2$ from the expression",
                      "left": "\\frac{ -2-2\\sqrt{ 10 } }{ 6 }",
                      "right": "\\frac{ 2\\left( -1-\\sqrt{ 10 } \\right) }{ 6 }"
                    }
                  ]
                },
                {
                  "headers": [
                    "Reduce the fraction"
                  ],
                  "substeps": [
                    {
                      "description": "Cancel out the common factor $2$ ",
                      "left": "\\frac{ 2\\left( -1-\\sqrt{ 10 } \\right) }{ 6 }",
                      "right": "\\frac{ -1-\\sqrt{ 10 } }{ 3 }"
                    }
                  ]
                }
              ]
            }
          }
        ]
      },
      {
        "headers": [
          "Check the solution"
        ],
        "substeps": [
          {
            "description": "Check if the solution is in the defined range ",
            "left": "\\begin{array} { l }\\begin{array} { l }m=\\frac{ -1+\\sqrt{ 10 } }{ 3 },\\\\m=\\frac{ -1-\\sqrt{ 10 } }{ 3 }\\end{array},& m≠0\\end{array}",
            "right": "\\begin{array} { l }m=\\frac{ -1+\\sqrt{ 10 } }{ 3 },\\\\m=\\frac{ -1-\\sqrt{ 10 } }{ 3 }\\end{array}"
          }
        ]
      },
      {
        "headers": [
          "The equation has $2$ solutions"
        ],
        "substeps": [
          {
            "description": "The equation has $2$ solutions, so we'll label them as $m_1$ and $m_2$",
            "left": "\\begin{array} { l }m=\\frac{ -1+\\sqrt{ 10 } }{ 3 },\\\\m=\\frac{ -1-\\sqrt{ 10 } }{ 3 }\\end{array}",
            "right": "\\begin{align*}&\\begin{array} { l }m_1=\\frac{ -1-\\sqrt{ 10 } }{ 3 },& m_2=\\frac{ -1+\\sqrt{ 10 } }{ 3 }\\end{array} \\\\&\\begin{array} { l }m_1\\approx-1.38743,& m_2\\approx0.720759\\end{array}\\end{align*}"
          }
        ]
      }
    ]
  },
  "info": {
    "solver": {
      "ioVersion": "9.0.0",
      "normalizedInput": {
        "action": {
          "command": "eq_solve",
          "args": [
            {
              "type": "var",
              "value": "m"
            }
          ]
        },
        "node": "\\frac{ 1 }{ 3 }=m+\\frac{ m-1 }{ m }"
      },
      "version": "11.11.12"
    }
  }
}