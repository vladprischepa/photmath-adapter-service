package com.phtomath.data.adapter

import com.phtomath.data.adapter.plugins.configureRouting
import io.ktor.server.engine.*
import io.ktor.server.netty.*

const val debugPort = 61231
const val releasePort = 80
fun main() {
    embeddedServer(Netty, port = debugPort) {
        configureRouting()
    }.start(wait = true)
}
