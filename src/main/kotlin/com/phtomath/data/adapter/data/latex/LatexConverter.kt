package com.phtomath.data.adapter.data.latex

import com.phtomath.data.adapter.safeArg

abstract class LatexConverter{
    abstract fun toLatex(vararg units: String): String?
}

class SymbolLatexConverter : LatexConverter(){
    override fun toLatex(vararg units: String): String? {
        return when(val unit = units.first()){
            null -> null
            "∞" -> "\\infty"
            else -> unit
        }
    }
}

class PreSymbolLatexConverter(val symbol: String): LatexConverter(){
    override fun toLatex(vararg units: String): String? {
        return try {
            val sb = StringBuilder(symbol)
            units.forEachIndexed { _, s -> sb.append(s)}
            sb.toString()
        }catch (e: Exception){
            println(e)
            null
        }
    }
}

class AfterSymbolLatexConverter(val symbol: String): LatexConverter(){
    override fun toLatex(vararg units: String): String? {
        return try {
            val sb = StringBuilder()
            units.forEachIndexed { _, s -> sb.append("$s $symbol")}
            sb.toString()
        }catch (e: Exception){
            println(e)
            null
        }
    }
}

class SimpleLatexConverter(val symbol: String) : LatexConverter() {
    override fun toLatex(vararg units: String): String? {
        return try {
            val sb = StringBuilder()
            units.forEachIndexed { index, s ->
                when(index){
                    0 -> sb.append(s)
                    else -> sb.append(symbol+s)
                }
            }
            sb.toString()
        }catch (e: Exception){
            println(e)
            null
        }
    }
}


class TwoSidedLatexConverter(
    val leftPattern: String,
    val rightPattern: String,
    val unitRule: String,
    var firstItemRule: String? = null,
    var lastItemRule: String? = null,
    var alignmentRule: AlignmentRule = AlignmentRule.NONE
): LatexConverter(){
    override fun toLatex(vararg units: String): String? {
        return try {
            val sb = StringBuilder()
            val left = applyAlignment(leftPattern, units.size)
            sb.append(left)
            units.forEachIndexed { i, s ->
                if (firstItemRule!=null && i==0){
                    sb.append(String.format(requireNotNull(firstItemRule), s))
                } else if (lastItemRule!=null && i == units.lastIndex){
                    sb.append(String.format(requireNotNull(lastItemRule), s))
                } else{
                    sb.append(String.format(unitRule, s))
                }
            }
            sb.append(rightPattern)
            sb.toString()
        }catch (e: Exception){
            println(e)
            null
        }
    }

    private fun applyAlignment(leftPattern: String, argsCount: Int): String{
        return try {
            val symbol = when(alignmentRule){
                AlignmentRule.LEFT -> "l"
                AlignmentRule.RIGHT -> "r"
                AlignmentRule.CENTER -> "c"
                else -> null
            }
            symbol?.let { s ->
                val sb = StringBuilder()
                for (i in 0 until argsCount){
                    sb.append(s)
                }
                sb.toString()
            }?.let { String.format(leftPattern, it) }
                ?: leftPattern
        }catch (e: Exception){
            e.printStackTrace()
            leftPattern
        }
    }
}
//UnitRule in string formatting format like "\\frac{%1$s}{%2$s}"
class PatternLatexConverter(val pattern: String): LatexConverter(){
    override fun toLatex(vararg units: String): String? {
        return try {
            String.format(pattern, *units)
        }catch (e: Exception){
            println(e)
            null
        }
    }
}

class FunctionLatexConverter(val inverse: Boolean = false): LatexConverter(){
    private val leftPattern: String = "%s\\left( "
    private val leftInversePattern: String = "%s^{-1}\\left( "
    private val rightPattern: String = " \\right)"
    override fun toLatex(vararg units: String): String? {
        return try {
            val sb = StringBuilder()
            units.forEachIndexed { index, s ->
                if (index==0){
                    if (inverse){
                        sb.append(String.format(leftInversePattern, s))
                    } else{
                        sb.append(String.format(leftPattern, s))
                    }
                } else if (units.size>2){
                    if (index==1){
                        sb.append(String.format("%s", s))
                    } else{
                        sb.append(String.format(", %s", s))
                    }
                } else{
                    sb.append(String.format("%s", s))
                }
            }
            sb.append(rightPattern)
            sb.toString()
        }catch (e: Exception){
            println(e)
            null
        }
    }
}

class DeterminantLatexConverter(var rowCount: Int): LatexConverter(){
    override fun toLatex(vararg units: String): String? {
        return try {
            val sb = StringBuilder("\\begin{vmatrix}\\\\")
            units.forEachIndexed { index, s ->
                if ((index+1)%rowCount!=0){
                    sb.append(String.format("%s & ", s))
                } else{
                    sb.append(String.format("%s\\\\", s))
                }
            }
            sb.append("\\end{vmatrix}")
            return sb.toString()
        }catch (e: Exception){
            println(e)
            null
        }
    }
}

class SarrusDeterminantLatexConverter(): LatexConverter(){
    override fun toLatex(vararg units: String): String? {
        return try {
            val detMatrix = units.slice(0..8)
            val sarrus = units.slice(9..units.lastIndex)
            val sb = StringBuilder("\\begin{vmatrix}\\\\")
            detMatrix.forEachIndexed { index, s ->
                if ((index+1)%3!=0){
                    sb.append(String.format("%s & ", s))
                } else{
                    sb.append(String.format("%s\\\\", s))
                }
            }
            sb.append("\\end{vmatrix}")
            sb.append(" \\begin{array}{l}\\\\")
            sarrus.forEachIndexed { index, s ->
                if ((index+1)%2!=0){
                    sb.append(String.format("%s & ", s))
                } else{
                    sb.append(String.format("%s\\\\", s))
                }
            }
            sb.append("\\end{array}")
            sb.toString()
        }catch (e: Exception){
            println(e)
            null
        }
    }
}

class LocalizedTextLatexConverter(): LatexConverter(){
    override fun toLatex(vararg units: String): String? {
        return try {
            if(units.size>3){
                val text = units.getOrNull(2)
                val args = units.takeLast(units.size-3)
                text?.let { header ->
                    TextNodeConverter.parseArgsAsLatex(header, args)
                }
            } else if (units.size==3){
                units.lastOrNull()?.let { String.format(TextNodeConverter.TEXT_STYLE_PATTERN, it) }
            }else null
        }catch (e:Exception){
            println(e)
            null
        }
    }
}

class PeriodicLocalizeConverter(): LatexConverter(){
    private val pattern = "\\overline{ %s }"
    override fun toLatex(vararg units: String): String? {
        return try {
            val number = units.firstOrNull()
            val overlineNumber = units.getOrNull(1)?.toInt().safeArg(1)
            number?.let { s ->
                buildString {
                    val s1 = s.substring(0, s.lastIndex-overlineNumber+1)
                    val s2 = s.substring(s.lastIndex-overlineNumber+1, s.lastIndex+1)
                    append(s1)
                    append(String.format(pattern, s2))
                }
            }?: number
        }catch (e: Exception){
            println(e)
            null
        }
    }
}

class PeriodicConverter(): LatexConverter(){
    private val pattern = "\\overset{ \\cdot }{ %s } "
    override fun toLatex(vararg units: String): String? {
        return try {
            val sb = StringBuilder()
            val number = units.firstOrNull()
            number?.let { s ->
                val overdotNumber = units.getOrNull(1)?.toInt()
                when(overdotNumber){
                    null, 1 -> {
                        sb.append(s.substring(0, s.lastIndex))
                        val last = s.last().toString()
                        val overdot = String.format(pattern, last)
                        sb.append(overdot)
                        sb.toString()
                    }
                    2 -> {
                        sb.append(s.substring(0, s.lastIndex-1))
                        val last = s.last().toString()
                        val beforeLast = s[s.lastIndex-1].toString()
                        sb.append(String.format(pattern, beforeLast))
                        sb.append(String.format(pattern, last))
                        sb.toString()
                    }
                    else ->  {
                        val overdotCharIndex = s.length-overdotNumber
                        val overDotChar = s[overdotCharIndex]
                        val firstSegment = s.substring(0, overdotCharIndex)
                        sb.append(firstSegment)
                        sb.append(String.format(pattern, overDotChar))
                        sb.append(s.substring(overdotCharIndex+1, s.lastIndex))
                        sb.append(String.format(pattern, s.last().toString()))
                        sb.toString()
                    }
                }
            }
        }catch (e:Exception){
            println(e)
            null
        }
    }

}

enum class AlignmentRule{
    LEFT, RIGHT, CENTER, NONE
}

