package com.phtomath.data.adapter.data.latex

import com.phtomath.data.adapter.data.model.CoreNode
import com.phtomath.data.adapter.data.model.NodeType

object LatexConverterFactory {
    //{x}^{2}-5x+3y=20
    fun createConverter(coreNode: CoreNode): LatexConverter?{
        return when(coreNode.type){
            NodeType.DIFFERENCE -> SimpleLatexConverter("\\setminus")
            NodeType.PERCENTAGE_OF -> PatternLatexConverter("{%1\$s}\\mathrm{\\%\\enspace of\\enspace}{ %2\$s }")
            //Text Fields
            NodeType.LOCALIZED_TEXT -> LocalizedTextLatexConverter()
            NodeType.TEXT -> SymbolLatexConverter()
            NodeType.TEXT_KEY -> SymbolLatexConverter()
            NodeType.STRING -> SymbolLatexConverter()
            //Simple
            NodeType.VARIABLE -> SymbolLatexConverter()
            NodeType.CONSTANT -> SymbolLatexConverter()
            NodeType.ENDEQUALS -> SymbolLatexConverter()
            NodeType.BLANK_BOX -> SymbolLatexConverter()
            NodeType.ADD -> SimpleLatexConverter("+")
            NodeType.ADD_SUBTRACT, NodeType.ADD_SUBTRACT_SIGN -> SimpleLatexConverter("\\pm")
            NodeType.SUBTRACT -> SimpleLatexConverter("-")
            NodeType.DIVIDE -> SimpleLatexConverter("\\div")
            NodeType.EQUALS, NodeType.EQUALS_SIGN -> SimpleLatexConverter("=")
            NodeType.UNIT -> SymbolLatexConverter()
            NodeType.NOT_EQUALS -> SimpleLatexConverter("≠")
            //Trigonometric functions
            //Cosine
            NodeType.COS -> PatternLatexConverter("\\cos\\left({%s}\\right)")
            NodeType.COSH -> PatternLatexConverter("\\cosh\\left({%s}\\right)")
            NodeType.ACOS -> PatternLatexConverter("\\arccos\\left({%s}\\right)")
            NodeType.ACOSH -> PatternLatexConverter("\\mathnormal{arcosh}\\left({%s}\\right)")
            //Sinus
            NodeType.SIN -> PatternLatexConverter("\\sin\\left({%s}\\right)")
            NodeType.ASIN -> PatternLatexConverter("\\arcsin\\left({%s}\\right)")
            NodeType.SINH -> PatternLatexConverter("\\sinh\\left({%s}\\right)")
            NodeType.ASINH -> PatternLatexConverter("\\mathnormal{arsinh}\\left({%s}\\right)")
            //Secant
            NodeType.SEC -> PatternLatexConverter("\\sec\\left({%s}\\right)")
            NodeType.SECH -> PatternLatexConverter("\\sech\\left({%s}\\right)")
            NodeType.ASEC -> PatternLatexConverter("\\mathnormal{arcsec}\\left({%s}\\right)")
            NodeType.ASECH -> PatternLatexConverter("\\mathnormal{arsech}\\left({%s}\\right)")
            //Cotangent
            NodeType.COT -> PatternLatexConverter("\\cot\\left({%s}\\right)")
            NodeType.COTH -> PatternLatexConverter("\\coth\\left({%s}\\right)")
            NodeType.ACOT -> PatternLatexConverter("\\mathnormal{arccot}\\left({%s}\\right)")
            NodeType.ACOTH -> PatternLatexConverter("\\mathnormal{arcoth}\\left({%s}\\right)")
            //Tangent
            NodeType.TAN, NodeType.TAN2 -> PatternLatexConverter("\\tan\\left({%s}\\right)")
            NodeType.TANH -> PatternLatexConverter("\\tanh\\left({%s}\\right)")
            NodeType.ATAN -> PatternLatexConverter("\\arctan\\left({%s}\\right)")
            NodeType.ATANH -> PatternLatexConverter("\\mathnormal{artanh}\\left({%s}\\right)")
            //Cosecant
            NodeType.CSC -> PatternLatexConverter("\\csc\\left({%s}\\right)")
            NodeType.CSCH -> PatternLatexConverter("\\mathnormal{csch}\\left({%s}\\right)")
            NodeType.ACSC -> PatternLatexConverter("\\mathnormal{arccsc}\\left({%s}\\right)")
            NodeType.ACSCH -> PatternLatexConverter("\\mathnormal{arcsch}\\left({%s}\\right)")
            //Degrees
            NodeType.RADIAN -> AfterSymbolLatexConverter("\\mathnormal{rad}")
            NodeType.DEGREE -> PatternLatexConverter("{%s}\\degree")
            NodeType.DEGREE_MINUTE -> PatternLatexConverter("{%s}\\degree{%s}^\\prime")
            NodeType.DEGREE_SECOND -> PatternLatexConverter("{%s}\\degree{%s}^\\prime\\prime")
            NodeType.DEGREE_MINUTE_SECOND -> PatternLatexConverter("{%s}\\degree{%s}^\\prime{%s}^{\\prime\\prime}")
            //Determinants
            NodeType.DETERMINANT2 -> DeterminantLatexConverter(2)
            NodeType.DETERMINANT3 -> DeterminantLatexConverter(3)
            NodeType.DETERMINANT4 -> DeterminantLatexConverter(4)
            NodeType.DETERMINANT5 -> DeterminantLatexConverter(5)
            NodeType.DETERMINANT3_SARRUS -> SarrusDeterminantLatexConverter()

            NodeType.CONJUGATE -> PatternLatexConverter("\\overline{ %s }")
            NodeType.INTERSECTION -> SimpleLatexConverter(" \\cap ")

            /**
             * Sets, Subsets & Supersets
             */
            NodeType.PROPER_SUPERSET -> SimpleLatexConverter(" \\supset ")
            NodeType.NOT_PROPER_SUPERSET -> SimpleLatexConverter(" \\not\\supset ")
            NodeType.SUPERSET_OR_EQUALS -> SimpleLatexConverter(" \\supseteq ")
            NodeType.NOT_PROPER_SUBSET -> SimpleLatexConverter("\\not\\subset ")
            NodeType.PROPER_SUBSET -> SimpleLatexConverter(" \\subset ")
            NodeType.SUBSET_OR_EQUALS -> SimpleLatexConverter(" \\subseteq ")
            NodeType.SET -> TwoSidedLatexConverter(leftPattern = "\\left\\{ ",
                rightPattern = " \\right\\}", unitRule = "%s, ", lastItemRule = "%s")


            NodeType.PERCENTAGE -> AfterSymbolLatexConverter("\\%")
            NodeType.POLAR -> PatternLatexConverter("{%s}\\angle{%s}")
            NodeType.ENDPOINTS -> TwoSidedLatexConverter(leftPattern = "", rightPattern = "", "%s")
            NodeType.VECTOR -> PatternLatexConverter("\\overrightarrow{ %s }")
            NodeType.ELEMENT_OF -> SimpleLatexConverter(" \\in ")
            NodeType.ELEMENT_NOT_OF -> SimpleLatexConverter("\\notin ")
            NodeType.MULTIPLY -> SimpleLatexConverter(" \\times ")
            NodeType.MULTIPLY_IMPLICIT -> SimpleLatexConverter("")
            NodeType.COMPOSITION -> SimpleLatexConverter(" \\circ ")
            NodeType.NEGATIVE -> PreSymbolLatexConverter("-")
            NodeType.POSITIVE -> PreSymbolLatexConverter("+")
            NodeType.ROOT -> PatternLatexConverter("\\sqrt[%s]{%s}")
            NodeType.ROOT2 -> PatternLatexConverter("\\sqrt{ %s }")
            NodeType.FRACTION -> PatternLatexConverter("\\frac{ %s }{ %s }")
            NodeType.FRACTION_MIXED -> PatternLatexConverter("%1\$s \\frac{ %2\$s }{ %3\$s }")
            NodeType.POWER -> PatternLatexConverter("{%s}^{%s}")
            NodeType.INTEGRAL -> PatternLatexConverter("\\int{ %1\$s } \\mathrm{d} %2\$s")
            NodeType.INTEGRAL_DEFINITE -> PatternLatexConverter("\\int_{ %1\$s }^{ %2\$s } %3\$s \\mathrm{d} %4\$s")
            NodeType.DERIVATION ->
                PatternLatexConverter("\\frac{ \\mathrm{d}^{%1\$s} }{ \\mathrm{d}%2\$s^{%1\$s}} \\left( %3\$s \\right)")
            NodeType.DERIVATION1 ->
                PatternLatexConverter("\\frac{ \\mathrm{d} }{ \\mathrm{d}%s} \\left( %s \\right)")
            NodeType.DERIVATION1_DIFF -> PatternLatexConverter("\\frac{ \\mathrm{d}%2\$s }{ \\mathrm{d}%2\$s }")
            NodeType.DERIVATION_DIFF ->
                PatternLatexConverter("\\frac{ \\mathrm{d}^%1\$s %2\$s }{ \\mathrm{d}%3\$s^%1\$s }")
            NodeType.ALTERNATIVE_FORM ->
                TwoSidedLatexConverter(
                    "\\begin{align*}",
                    "\\end{align*}",
                    "&%s \\\\",
                    lastItemRule = "&%s"
                )
            NodeType.VERTICAL_LIST ->
                TwoSidedLatexConverter(
                    "\\begin{array} { l }",
                    "\\end{array}",
                    "%s,\\\\",
                    lastItemRule = "%s",
                )
            NodeType.LIST ->
                TwoSidedLatexConverter(
                    "\\begin{array} { l }",
                    "\\end{array}",
                    "%s,& ",
                    lastItemRule = "%s"
                )
            NodeType.APPROXIMATE -> SimpleLatexConverter("\\approx")
            NodeType.APPROXIMATE_SIGN -> PreSymbolLatexConverter("\\approx")
            NodeType.BRACKET -> PatternLatexConverter("\\left( %s \\right)")
            NodeType.LIMIT -> PatternLatexConverter("\\lim_{%1\$s \\rightarrow %2\$s} \\left(%3\$s\\right)")
            NodeType.LIMIT_RIGHT -> PatternLatexConverter("\\lim_{%1\$s \\rightarrow %2\$s^+} \\left(%3\$s\\right)")
            NodeType.LIMIT_LEFT -> PatternLatexConverter("\\lim_{%1\$s \\rightarrow %2\$s^-} \\left(%3\$s\\right)")
            NodeType.LESS_THAN_EQUAL -> SimpleLatexConverter(" \\leq ")
            NodeType.GREATER_THAN -> SimpleLatexConverter(" > ")
            NodeType.LESS_THAN -> SimpleLatexConverter(" < ")
            NodeType.GREATER_THAN_EQUAL -> SimpleLatexConverter(" \\geq ")
            NodeType.UNION -> SimpleLatexConverter(" \\cup ")
            NodeType.OPEN_OPEN_INTERVAL ->
                TwoSidedLatexConverter("\\langle", "\\rangle", "%s, ", lastItemRule = "%s")
            NodeType.CLOSED_OPEN_INTERVAL ->
                TwoSidedLatexConverter("\\left[ ", "\\right\\rangle", "%s, ", lastItemRule = "%s")
            NodeType.CLOSED_CLOSED_INTERVAL ->
                TwoSidedLatexConverter("\\left[ ", "\\right]", "%s, ", lastItemRule = "%s")
            NodeType.OPEN_CLOSED_INTERVAL ->
                TwoSidedLatexConverter("\\left\\langle", "\\right]", "%s, ", lastItemRule = "%s")
            NodeType.MATRIX ->
                TwoSidedLatexConverter("\\left[\\begin{matrix} ", "\\end{matrix}\\right]", "%s \\\\ ", lastItemRule = "%s")
            NodeType.MATRIX_ROW -> TwoSidedLatexConverter(" ", " ", " & %s", firstItemRule = "%s")
            NodeType.SYSTEM ->
                TwoSidedLatexConverter("\\left\\{\\begin{array} { l } ", "\\end{array} \\right.", "%s \\\\ ", lastItemRule = "%s")
            NodeType.ORDER ->
                TwoSidedLatexConverter("\\left( ", "\\right)", ", %s", firstItemRule = "%s")
            NodeType.LOG -> PatternLatexConverter("\\log_{ %1\$s }({ %2\$s })")
            NodeType.LOG10 -> PatternLatexConverter("\\log( %s )")
            NodeType.SIGMA_DEFINITE -> PatternLatexConverter("\\sum_{ %1\$s=%2\$s }^{ %3\$s } %4\$s")

            NodeType.FUNCTION -> FunctionLatexConverter()
            NodeType.FUNCTION_INVERSE -> FunctionLatexConverter(true)
            NodeType.FUNCTION_OPERATION -> PatternLatexConverter("\\left( %1\$s \\right)\\left( %2\$s \\right)")
            NodeType.DERIVATION_PRIME -> PatternLatexConverter("%2\$s ^{(%1\$s)}")
            NodeType.DERIVATION1_PRIME -> AfterSymbolLatexConverter("'")
            NodeType.DERIVATION2_PRIME -> AfterSymbolLatexConverter("''")
            NodeType.FACTORIAL -> AfterSymbolLatexConverter("!")
            NodeType.PARTIAL_PERMUTATION_LEFT_SUBSCRIPT_RIGHT_SUBSCRIPT ->
                PatternLatexConverter("_{ %1\$s } P_{ %2\$s }")
            NodeType.CHOOSE_LEFT_SUBSCRIPT_RIGHT_SUBSCRIPT -> PatternLatexConverter("_{ %1\$s } C_{ %2\$s }")
            NodeType.VARIATION -> PatternLatexConverter("_{ %2\$s } V_{ %1\$s }")
            NodeType.ABS -> PatternLatexConverter("|%s|")
            NodeType.PARTIAL_DERIVATION1 -> PatternLatexConverter("\\frac{ \\partial }{ \\partial %s } \\left( %s \\right)")
            NodeType.PARTIAL_DERIVATION1_DIFF -> PatternLatexConverter("\\frac{ \\partial %s }{ \\partial %s }")
            NodeType.INTEGRAL_RIGHT_DASH -> PatternLatexConverter("\\left. %1\$s \\right|_{ %2\$s }^{ %3\$s }")
            NodeType.CHOOSE -> PatternLatexConverter("\\binom{ %s }{ %s }")
            NodeType.FLOOR -> PatternLatexConverter("\\lfloor %s \\rfloor")
            NodeType.DIFFERENTIAL -> PatternLatexConverter("\\mathrm{d}^%s")
            NodeType.LN -> PatternLatexConverter("\\ln\\left({%s}\\right)")
            NodeType.PERIODIC -> PeriodicConverter()
            NodeType.TRIANGLE -> TwoSidedLatexConverter(leftPattern = "\\triangle{ ", rightPattern = "}", unitRule = "%s")
            NodeType.PERIODIC_LOCALIZE -> PeriodicLocalizeConverter()
            NodeType.PERIODIC_VINCULUM -> PeriodicLocalizeConverter()
            NodeType.ANGLE_BRACKET_LIST -> TwoSidedLatexConverter("\\left\\langle", "\\right\\rangle", "%s, ", lastItemRule = "%s")
            //Conditional
            NodeType.PIECEWISE_DEFINITION ->
                TwoSidedLatexConverter("\\left\\{\\begin{array} {l}", "\\end{array}\\right.", "%s\\\\")
            NodeType.CONDITIONAL_EXPRESSION -> TwoSidedLatexConverter("", "", ", & %s", firstItemRule = "%s")
            NodeType.INDEXED -> PatternLatexConverter("{ %1\$s }_{ %2\$s }")
            NodeType.POINT -> PatternLatexConverter("{ %1\$s } { %2\$s }")
            else -> {
                println("LatexConverterFactory: unsupported core node - ${coreNode.type}")
                null
            }
        }
    }
}