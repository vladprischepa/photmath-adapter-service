package com.phtomath.data.adapter.data.latex

import com.phtomath.data.adapter.data.model.CoreNode


object NodeToLatexConverter {
    //{x}^{2}-5x+3y=20

    fun nodeToLatexString(node: CoreNode): String? {
        val converter = LatexConverterFactory.createConverter(node)
        return if (node.hasChildren){
            val childStrings = node.children?.map { childNode ->
                if (childNode.hasChildren) {
                    nodeToLatexString(childNode)
                } else {
                    childNode.value?.let {
                        val conv = LatexConverterFactory.createConverter(childNode)
                        conv?.toLatex(it)
                    }
                }
            }
            childStrings?.filterNotNull()?.toTypedArray()?.let { converter?.toLatex(*it) }
        } else {
            node.value?.let {
                converter?.toLatex(it)
            }
        }
    }
}