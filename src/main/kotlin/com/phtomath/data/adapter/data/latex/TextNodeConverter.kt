package com.phtomath.data.adapter.data.latex

import com.phtomath.data.adapter.data.model.TextNode

object TextNodeConverter {

    private const val UUID_IN_BRACKETS_PATTERN = "\\[([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})]"
    private const val ANY_IN_BRACKETS_PATTERN = "\\[(.*?)]"
    private const val ARG_PATTERN = "([ARG])+\\d+"

    const val TEXT_STYLE_PATTERN = "\\textnormal{%s}"

    fun textNodeToString(textNode: TextNode?): String? {
        return try {
            textNode?.let { tNode ->
                tNode.localizedText?.text?.takeIf { it.isNotBlank() }
                    ?.let { clearLinks(it) }
                    ?.let { localizedText ->
                        tNode.args.takeIf { it.isNotEmpty() }
                            ?.mapNotNull { NodeToLatexConverter.nodeToLatexString(it) }
                            ?.let { args ->
                                parseArgs(localizedText, args)
                            } ?: localizedText
                    }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            println(e.localizedMessage ?: e::class.java.simpleName)
            null
        }
    }

    private fun clearLinks(text: String): String? {
        return try {
            text.replace(Regex(UUID_IN_BRACKETS_PATTERN)) { "" }
                .replace(Regex(ANY_IN_BRACKETS_PATTERN)) { matchResult: MatchResult ->
                    val match = matchResult.value
                    match.replace("[", "").replace("]", "")
                        .substringBefore("|")
                }
        } catch (e: Exception) {
            e.printStackTrace()
            println(e.localizedMessage ?: e::class.java.simpleName)
            null
        }
    }

    private fun parseArgs(text: String, args: List<String>): String? {
        return try {
            text.replace(Regex(ARG_PATTERN)) { matchResult: MatchResult ->
                val match = matchResult.value
                val index = match.substringAfter("ARG").toInt()
                val argument = args[index - 1]
                "$${argument}$"
            }
        } catch (e: Exception) {
            e.printStackTrace()
            println(e.localizedMessage ?: e::class.java.simpleName)
            println("Failed to parse args: $text")
            null
        }
    }

    fun parseArgsAsLatex(text: String, args: List<String>): String? {
        return try {
            var edited = text
            Regex(ARG_PATTERN).split(edited).forEach { s ->
                if (s.isNotBlank()){
                    edited = edited.replace(s, String.format(TEXT_STYLE_PATTERN, s))
                }
            }
            edited.replace(Regex(ARG_PATTERN)) { matchResult: MatchResult ->
                val match = matchResult.value
                val index = match.substringAfter("ARG").toInt()
                val argument = args[index - 1]
                argument
            }
        } catch (e: Exception) {
            e.printStackTrace()
            println(e.localizedMessage ?: e::class.java.simpleName)
            null
        }
    }
}