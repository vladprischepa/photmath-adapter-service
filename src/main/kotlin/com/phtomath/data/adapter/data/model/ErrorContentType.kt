package com.phtomath.data.adapter.data.model

import com.google.gson.annotations.SerializedName

enum class ErrorContentType {
    @SerializedName("blurred")
    BLURRED,
    @SerializedName("junk")
    JUNK
}