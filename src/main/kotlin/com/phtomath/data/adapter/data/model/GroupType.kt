package com.phtomath.data.adapter.data.model

import com.google.gson.annotations.SerializedName
import com.phtomath.data.adapter.safeArg


enum class GroupType(val key:String) {
    @SerializedName("animation")
    ANIMATION("animation"),
    @SerializedName("bookpoint")
    BOOKPOINT("bookpoint"),
    @SerializedName("graph")
    GRAPH("graph"),
    @SerializedName("problem-search")
    PROBLEM_SEARCH("problem-search"),
    @SerializedName("vertical")
    VERTICAL("vertical"),
    @SerializedName("unknown")
    UNKNOWN("unknown");

    companion object{
        fun parse(value: String?): GroupType{
            return values().find { it.key == value }.safeArg(UNKNOWN)
        }
    }
}