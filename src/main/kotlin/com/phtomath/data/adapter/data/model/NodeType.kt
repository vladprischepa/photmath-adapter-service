@file:Suppress("ClassName")

package com.phtomath.data.adapter.data.model

import kotlin.reflect.full.isSubclassOf


sealed class NodeType(val key: String) {
    object DIFFERENCE: NodeType("diff")
    object COMPOSITION: NodeType("composition")
    object FUNCTION_OPERATION: NodeType("function_operation")
    object PERCENTAGE_OF: NodeType("percentageof")
    object TRIANGLE: NodeType("angle")
    object ANGLE_BRACKET_LIST: NodeType("angle_bracket_list")
    object PERIODIC_LOCALIZE: NodeType("periodic_localize")
    object PERIODIC_VINCULUM: NodeType("periodic_vinculum")
    object POLAR: NodeType("polar")
    object ENDPOINTS: NodeType("endpoints")
    object VECTOR: NodeType("vec")
    object BLANK_BOX: NodeType("blank_box")
    object ENDEQUALS: NodeType("endequals")
    object POINT: NodeType("point")
    object INDEXED: NodeType("indexed")

    object ABS : NodeType("abs")

    object ACOS : NodeType("acos")

    object ACOSH : NodeType("acosh")

    object ACOT : NodeType("acot")

    object ACOTH : NodeType("acoth")

    object ACSC : NodeType("acsc")

    object ACSCH : NodeType("acsch")

    object ADD : NodeType("add")

    object ADD_SUBTRACT : NodeType("add_sub")

    object ADD_SUBTRACT_SIGN : NodeType("add_sub_sign")

    object ALTERNATIVE_FORM : NodeType("alt_form")

    object APPROXIMATE : NodeType("approx")

    object APPROXIMATE_SIGN : NodeType("approx_sign")

    object ASEC : NodeType("asec")

    object ASECH : NodeType("asech")

    object ASIN : NodeType("asin")

    object ASINH : NodeType("asinh")

    object ATAN : NodeType("atan")

    object ATANH : NodeType("atanh")

    object BRACKET : NodeType("bracket")

    object CHOOSE : NodeType("choose")

    object CHOOSE_LEFT_SUBSCRIPT_RIGHT_SUBSCRIPT : NodeType("choose_lsub_rsub")

    object CHOOSE_LOCALIZE : NodeType("choose_localize")

    object CLOSED_CLOSED_INTERVAL : NodeType("ccint")

    object CLOSED_OPEN_INTERVAL : NodeType("coint")

//    object CONDITIONAL_DEFINITION : NodeType("cond_def")

    object CONDITIONAL_EXPRESSION : NodeType("cond_expr")

    object CONJUGATE : NodeType("conj")

    object CONSTANT : NodeType("const")

    object COS : NodeType("cos")

    object COSH : NodeType("cosh")

    object COT : NodeType("cot")

    object COTH : NodeType("coth")

    object CSC : NodeType("csc")

    object CSCH : NodeType("csch")

    object DEGREE : NodeType("deg")

    object DEGREE_MINUTE : NodeType("degmin")

    object DEGREE_MINUTE_SECOND : NodeType("degminsecond")

    object DEGREE_SECOND : NodeType("degsecond")

    object DERIVATION : NodeType("nderivation")

    object DERIVATION1 : NodeType("derivation")

    object DERIVATION1_DIFF : NodeType("derivation_diff")

    object DERIVATION_PRIME : NodeType("nderivationprime")

    object DERIVATION1_PRIME : NodeType("derivationprime")

    object DERIVATION2_PRIME : NodeType("derivationprime2")

    object DERIVATION_DIFF : NodeType("nderivation_diff")

//    object DETERMINANT : NodeType("det")

    object DETERMINANT2 : NodeType("det2")

    object DETERMINANT3 : NodeType("det3")

    object DETERMINANT3_SARRUS : NodeType("det3s")

    object DETERMINANT4 : NodeType("det4")

    object DETERMINANT5 : NodeType("det5")


    object DIFFERENTIAL : NodeType("differential")

    object DIVIDE : NodeType("div")

    object ELEMENT_NOT_OF : NodeType("elem_not_of")

    object ELEMENT_OF : NodeType("elem_of")

//    object EMPTY : NodeType("empty")

    object EQUALS : NodeType("equals")
    object EQUALS_SIGN : NodeType("equals")

//    object EVALUATE_EXPRESSION : NodeType("eval_expr")

//    object EXP : NodeType("exp")

    object FACTORIAL : NodeType("factorial")

    object FLOOR : NodeType("floor")

    object FRACTION : NodeType("frac")

    object FRACTION_MIXED : NodeType("mixedfrac")

    object FUNCTION : NodeType("function")

    object FUNCTION_INVERSE : NodeType("function_inverse")

    object GREATER_THAN : NodeType("gt")

    object GREATER_THAN_EQUAL : NodeType("gte")

//    object HORIZONTAL_MULTIPLY : NodeType("horizontalmul")

//    object IMAGINARY : NodeType("imag")

    object INTEGRAL : NodeType("integral")

    object INTEGRAL_DEFINITE : NodeType("definiteintegral")

    object INTEGRAL_RIGHT_DASH : NodeType("integralrightdash")

    object INTERSECTION : NodeType("intsec")

    object LESS_THAN : NodeType("lt")

    object LESS_THAN_EQUAL : NodeType("lte")

    object LIMIT : NodeType("lim")

    object LIMIT_LEFT : NodeType("lim_left")

    object LIMIT_RIGHT : NodeType("lim_right")

//    object LINE_SEGMENT : NodeType("line_segment")

//    object LINE_SEGMENT_LENGTH : NodeType("line_segment_length")

    object LIST : NodeType("list")

    object LN : NodeType("ln")

    object LOCALIZED_TEXT : NodeType("localized_text")

    object LOG : NodeType("log")

    object LOG10 : NodeType("log10")

    object MATRIX : NodeType("mat")

    object MATRIX_ROW : NodeType("mat_row")

//    object MATRIX_SPLIT_CENTER : NodeType("mat_split_center")

//    object MATRIX_SPLIT_LAST : NodeType("mat_split_last")

//    object MEAN : NodeType("mean")

//    object MINUTE : NodeType("min")

//    object MINUTE_SECOND : NodeType("minsecond")

    object MULTIPLY : NodeType("mul")

    object MULTIPLY_IMPLICIT : NodeType("muli")

    object NEGATIVE : NodeType("negative")

    object NOT_EQUALS : NodeType("not_equals")

    object NOT_PROPER_SUBSET : NodeType("not_proper_subset")

    object NOT_PROPER_SUPERSET : NodeType("not_proper_superset")

    object OPEN_CLOSED_INTERVAL : NodeType("ocint")

    object OPEN_OPEN_INTERVAL : NodeType("ooint")

    object ORDER : NodeType("order")

    object PARTIAL_DERIVATION1 : NodeType("partial_derivation")

    object PARTIAL_DERIVATION1_DIFF : NodeType("partial_derivation_diff")

    object PARTIAL_PERMUTATION_LEFT_SUBSCRIPT_RIGHT_SUBSCRIPT : NodeType("partial_permutation_lsub_rsub")

    object PERCENTAGE : NodeType("percentage")

    object PERIODIC : NodeType("periodic")

    object PIECEWISE_DEFINITION : NodeType("piecewise_def")

    object POSITIVE : NodeType("positive")

    object POWER : NodeType("pow")

//    object PRIME : NodeType("prime")

    object PROPER_SUBSET : NodeType("proper_subset")

    object PROPER_SUPERSET : NodeType("proper_superset")

    object RADIAN : NodeType("radian")

    object REAL : NodeType("real")

    object ROOT : NodeType("root")

    object ROOT2 : NodeType("root2")

//    object RU_LONGDIV : NodeType("rulongdiv")

    object SEC : NodeType("sec")

    object SECH : NodeType("sech")

//    object SECOND : NodeType("second")

//    object SEQUENCE : NodeType("sequence")

    object SET : NodeType("set")

    object SIGMA_DEFINITE : NodeType("definitesigma")

//    object SIGN : NodeType("sign")

    object SIN : NodeType("sin")

    object SINH : NodeType("sinh")

    object STRING : NodeType("string")

    object SUBSET_OR_EQUALS : NodeType("subset_or_eq")

    object SUBTRACT : NodeType("sub")

    object SUPERSET_OR_EQUALS : NodeType("superset_or_eq")

    object SYSTEM : NodeType("system")

    object TAN : NodeType("tan")
    object TAN2 : NodeType("tn")

    object TANH : NodeType("tanh")

    object TEXT : NodeType("text")

    object TEXT_KEY : NodeType("text_key")

    object UNION : NodeType("union")

//    object US_LONGDIV : NodeType("uslongdiv")

    object VARIABLE : NodeType("var")

    object VARIATION : NodeType("variation")

//    object VARIATION_A : NodeType("variation_a")

//    object VERTICAL_ADD : NodeType("verticaladd")

//    object VERTICAL_DIVIDE : NodeType("verticaldiv")

    object VERTICAL_LIST : NodeType("vert_list")

//    object VERTICAL_MULTIPLY : NodeType("verticalmul")

//    object VERTICAL_SUBTRACT : NodeType("verticalsub")

    object UNIT : NodeType("unit")

    class Unknown(
        key: String,
        var nodeJsonString: String? = null,
        var fullExpressionJson: String? = null
    ) : NodeType(key)

    object Undefined: NodeType("undefined")

    companion object {
        @JvmStatic
        private val values = NodeType::class.nestedClasses
            .filter { klass -> klass.isSubclassOf(NodeType::class) }
            .map { klass -> klass.objectInstance }
            .filterIsInstance<NodeType>()
            .associateBy { value -> value.key }
            .filter { it.value !is Unknown }
            .filter { it.value !is Undefined }

        @JvmStatic
        fun parse(typeString: String): NodeType {
            return values[typeString] ?: Unknown(typeString)
        }
    }
}