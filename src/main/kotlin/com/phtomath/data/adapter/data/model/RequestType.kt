package com.phtomath.data.adapter.data.model

enum class RequestType {
    RecognizeImage, StepsRequest, HealthCheck
}