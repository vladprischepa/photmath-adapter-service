package com.phtomath.data.adapter.data.model

import com.google.gson.annotations.SerializedName

enum class SolverType{
    @SerializedName("animation")
    ANIMATION,
    @SerializedName("graph")
    GRAPH,
    @SerializedName("vertical")
    VERTICAL,
    @SerializedName("unknown")
    UNKNOWN
}