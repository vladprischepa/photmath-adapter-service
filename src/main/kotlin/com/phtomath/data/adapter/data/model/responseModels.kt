package com.phtomath.data.adapter.data.model

import com.google.gson.annotations.SerializedName

data class ImageRecognizeResponse(
    @SerializedName("result") val result: Result?,
    @SerializedName("info") val info: Info?
)

data class StepsResponse(
    @SerializedName("result") val result: StepsResult?,
    @SerializedName("info") val info: Info?
)

data class StepsResult(
    @SerializedName("header") val header: TextNode?,
    @SerializedName("solution") val solution: CoreNode?,
    @SerializedName("steps") val steps: List<CoreSolverStep>?
)

data class CoreSolverStep(
    @SerializedName("headers") val headers: List<TextNode>?,
    @SerializedName("substeps") val substeps: List<CoreSolverSubstep>?
)

data class CoreSolverSubstep(
    @SerializedName("description") val description: TextNode?,
    @SerializedName("left") val left: CoreNode?,
    @SerializedName("right") val right: CoreNode?,
    @SerializedName("subresult") val subresult: StepsResult?
)

data class Result(
    @SerializedName("solutions", alternate = ["groups"]) var groups: List<VerticalCoreGroup> = emptyList()
)

data class CoreNode(
    @SerializedName("children") var children: List<CoreNode>?,
    @SerializedName("type") var type: NodeType = NodeType.Undefined,
    @SerializedName("value") val value: String?,
    @SerializedName("color") val color: Int?
) {
    val hasChildren: Boolean get() = !children.isNullOrEmpty()
}

data class NodeAction(
    @SerializedName("action") val action: Action?,
    @SerializedName("node") var node: CoreNode?,
)

data class CommandNode(
    @SerializedName("action") val action: Action?,
    @SerializedName("node") var node: CoreNode?
)
data class Action(
    @SerializedName("command") val command: String?,
    @SerializedName("args") var args: List<CoreNode> = emptyList()
)

open class CoreGroup(){
    open var type: GroupType = GroupType.UNKNOWN
}

data class VerticalCoreGroup(
    @SerializedName("variants", alternate = ["entries"]) var entries: List<VerticalEntry>
) : CoreGroup()

data class VerticalPreview(
    @SerializedName("content") val content: PreviewContent?,
    @SerializedName("method") val method: TextNode?,
    @SerializedName("title") val title: TextNode?
)

data class VerticalEntry(
    @SerializedName("stepsRequestJSON", alternate = ["nodeAction"]) var nodeAction: NodeAction?,
    @SerializedName("preview") var preview: VerticalPreview?
)

data class PreviewContent(
    @SerializedName("description") val description: TextNode?,
    @SerializedName("problem") val problem: CoreNode?,
    @SerializedName("solution") val solution: CoreNode?
){

}

open class Preview {
    @SerializedName("title")
    var title: TextNode? = null
}

data class TextNode(
    @SerializedName("type") val type: String?,
    @SerializedName("args") var args: List<CoreNode> = emptyList(),
    @SerializedName("voiceKey") val voiceKey: String?,
    @SerializedName("localizedText") var localizedText: Translation?,
    @SerializedName("localizedVoiceText") var localizedVoiceText: Translation?
)

data class Translation(
    @SerializedName("locale") val locale: String?,
    @SerializedName("text") val text: String? = null
)

data class Info(
    @SerializedName("solver") val solver: SolverInfo?,
    @SerializedName("imageToMath") val imageToMath: ImageToMathInfo?,
    @SerializedName("writingTypeClassifier") val writingTypeClassifier: WritingTypeClassifier?
)

data class SolverInfo(
    @SerializedName("errorType") val errorType: String?,
    @SerializedName("ioVersion") val ioVersion: String?,
    @SerializedName("normalizedInput") val normalizedInput: CommandNode?,
    @SerializedName("version") val version: String?
)

data class ImageToMathInfo(
    @SerializedName("command")
    var command: CommandNode?,
    @SerializedName("contentType")
    var contentType: String?,
    @SerializedName("version")
    var version: String?,
    @SerializedName("fineGrainedContentType")
    var fineGrainedContentType: String?
)

data class WritingTypeClassifier(
    @SerializedName("version") val version: String?,
    @SerializedName("type")val type: String?,
    @SerializedName("score") val score: Float?
)