package com.phtomath.data.adapter.data.serialization

import com.google.gson.*
import com.phtomath.data.adapter.data.model.CoreNode
import com.phtomath.data.adapter.data.model.NodeType
import com.phtomath.data.adapter.data.serialization.deserializers.SerializationKeys
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeArg
import com.phtomath.data.adapter.safeMessage
import io.ktor.util.logging.*
import java.lang.reflect.Type


private const val LOG_TAG = "CoreNodeDeserializer: "
object CoreNodeJsonSerializer : JsonDeserializer<CoreNode?>, JsonSerializer<CoreNode> {
    private val gson = Gson()
    private lateinit var logger: Logger
    private lateinit var analytics: Analytics

    fun injectLogger(logger: Logger, analytics: Analytics){
        this.logger = logger
        this.analytics = analytics
    }

    override fun deserialize(element: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): CoreNode? {
        return parseCoreNode(element)
    }

    private fun parseCoreNode(element: JsonElement?): CoreNode{
        val methodTag = "parseCoreNode:"
        return try {
            element?.asJsonObject?.let { obj ->
                val type: NodeType = obj.getAsJsonPrimitive("type")
                    ?.asString
                    ?.let { NodeType.parse(it) }
                    .safeArg(NodeType.Undefined)
                if (type is NodeType.Unknown){
                    val nodeJsonString = element.let { gson.toJson(it) }
                    type.nodeJsonString = nodeJsonString
                    logger.error("Unknown Node: ${type.key}")
                    logger.error("Unknown Node: ${type.nodeJsonString}")
//                    analytics.sendUnrecognizedCoreNodeEvent(type)
                }
                val value: String? = obj.getAsJsonPrimitive("value")?.asString
                val color: Int? = obj.getAsJsonPrimitive("color")?.asInt
                val children: List<CoreNode> = obj.getAsJsonArray("children")
                    ?.map { parseCoreNode(it) }.safeArg(emptyList())
                CoreNode(
                    children,
                    type,
                    value,
                    color
                )
            }?: parseErrorNode(element)
        }catch (e: Exception){
            logger.error("$LOG_TAG$methodTag", e.safeMessage())
            parseErrorNode(element)
        }
    }

    private fun parseErrorNode(element: JsonElement?): CoreNode{
        val nodeJsonString = element?.let { gson.toJson(it) }
        val type: NodeType = element?.asJsonObject
            ?.getAsJsonPrimitive("type")
            ?.asString
            ?.let { NodeType.parse(it) }
            .safeArg(NodeType.Undefined)
        if (type is NodeType.Unknown){
            type.nodeJsonString = nodeJsonString
            logger.error("Unknown Node: ${type.key}")
            logger.error("Unknown Node: ${type.nodeJsonString}")
//            analytics.sendUnrecognizedCoreNodeEvent(type)
        }
        val value = element?.asJsonObject?.getAsJsonPrimitive("value")?.asString
        return CoreNode(children = null, type = type, value = value, color = null)
    }

    override fun serialize(src: CoreNode?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
        return try {
            when(src){
                null -> JsonNull.INSTANCE
                else -> {
                    serializeNode(src)
                }
            }
        }catch (e: Exception){
            println(e)
            JsonNull.INSTANCE
        }
    }
}

private fun serializeNode(coreNode: CoreNode): JsonElement{
    return JsonObject().apply {
        val childrenArray = JsonArray()
        coreNode.children?.forEach { childNode ->
            childrenArray.add(serializeNode(childNode))
        }
        coreNode.children?.takeIf { it.isNotEmpty() }?.also {
            add(SerializationKeys.KEY_CHILDREN, childrenArray)
        }
        addProperty(SerializationKeys.KEY_TYPE, coreNode.type.key)
        coreNode.color?.also { addProperty(SerializationKeys.KEY_COLOR, it) }
        coreNode.value?.also { addProperty(SerializationKeys.KEY_VALUE, it) }

    }
}