package com.phtomath.data.adapter.data.serialization

import com.google.gson.GsonBuilder
import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter
import com.phtomath.data.adapter.data.model.CoreNode
import com.phtomath.data.adapter.data.model.NodeAction

class NodeActionToJsonResponseTypeAdapter : TypeAdapter<String?>() {
    private val gson = GsonBuilder()
        .disableHtmlEscaping()
        .registerTypeAdapter(CoreNode::class.java, CoreNodeJsonSerializer)
        .registerTypeAdapter(NodeAction::class.java, NodeJsonActionSerializer)
        .create()
    override fun write(out: JsonWriter?, value: String?) {
        out?.let { writer -> value?.let { s-> writer.value(s) }?:writer.nullValue() }
    }

    override fun read(reader: JsonReader?): String? {
        return try {
            when(reader?.peek()){
                null, JsonToken.NULL -> {
                    reader?.nextNull()
                    null
                }
                JsonToken.BEGIN_OBJECT -> {
                    val node = gson.fromJson<NodeAction>(reader, NodeAction::class.java)
                    gson.toJson(node)
                }
                else -> null
            }
        }catch (e: Exception){
            e.printStackTrace()
            null
        }
    }
}