package com.phtomath.data.adapter.data.serialization

import com.google.gson.*
import com.phtomath.data.adapter.data.model.CoreNode
import com.phtomath.data.adapter.data.model.NodeAction
import java.lang.reflect.Type

object NodeJsonActionSerializer :  JsonSerializer<NodeAction?>{
    private val gson = GsonBuilder()
        .registerTypeAdapter(CoreNode::class.java, CoreNodeJsonSerializer)
        .create()
    override fun serialize(src: NodeAction?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
        return try {
            when(src){
                null -> JsonNull.INSTANCE
                else -> {
                    val element = gson.toJsonTree(src, NodeAction::class.java)
                    element.asJsonObject.add("experiments", JsonObject())
                    element
                }
            }
        }catch (e: Exception){
            println(e)
            JsonNull.INSTANCE
        }
    }
}