package com.phtomath.data.adapter.data.serialization

import com.google.gson.GsonBuilder
import com.google.gson.TypeAdapter
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter
import com.phtomath.data.adapter.data.latex.NodeToLatexConverter
import com.phtomath.data.adapter.data.model.CoreNode

class NodeToLaTeXListTypeAdapter : TypeAdapter<List<String>?>() {

    val gson = GsonBuilder()
        .registerTypeAdapter(CoreNode::class.java, CoreNodeJsonSerializer)
        .create()


    override fun write(out: JsonWriter?, value: List<String>?) {
        out?.let { writer ->
            value?.let { list ->
                writer.beginArray()
                list.forEach {
                    writer.value(it)
                }
                writer.endArray()
            }
        } ?: out?.nullValue()
    }

    override fun read(reader: JsonReader?): List<String>? {
        return try {
            when(reader?.peek()){
                null, JsonToken.NULL -> {
                    reader?.nextNull()
                    null
                }
                JsonToken.BEGIN_ARRAY -> {
                    val typeToken = object : TypeToken<List<CoreNode>>(){}.type
                    val nodes = gson.fromJson<List<CoreNode>>(reader, typeToken)
                    nodes.mapNotNull { NodeToLatexConverter.nodeToLatexString(it) }
                }
                else -> null
            }
        }catch (e:Exception){
            e.printStackTrace()
            null
        }
    }
}