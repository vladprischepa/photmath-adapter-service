package com.phtomath.data.adapter.data.serialization

import com.google.gson.GsonBuilder
import com.google.gson.JsonParser
import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter
import com.phtomath.data.adapter.data.latex.NodeToLatexConverter
import com.phtomath.data.adapter.data.model.CoreNode
import com.phtomath.data.adapter.safeMessage

class NodeToLaTeXTypeAdapter() : TypeAdapter<String?>() {
    private val gson = GsonBuilder()
        .registerTypeAdapter(CoreNode::class.java, CoreNodeJsonSerializer)
        .setPrettyPrinting()
        .setLenient()
        .create()
    override fun write(out: JsonWriter?, value: String?) {
        out?.let { writer ->
            value?.let { s->
                writer.value(s)
            }?:writer.nullValue()
        }
    }

    override fun read(reader: JsonReader?): String? {
        return try {
            when(reader?.peek()){
                null, JsonToken.NULL -> {
                    reader?.nextNull()
                    null
                }
                JsonToken.BEGIN_OBJECT -> {
                    tryConvertNode(reader)?.let { NodeToLatexConverter.nodeToLatexString(it) }
                }
                else -> null
            }
        }catch (e: Exception){
            e.printStackTrace()
            reader?.let { logReader(it) }
            null
        }
    }

    private fun tryConvertNode(reader: JsonReader): CoreNode?{
        return try {
            gson.fromJson<CoreNode>(reader, CoreNode::class.java)
        }catch (e: Exception){
            println("NodeToLaTeXTypeAdapter: ${e.safeMessage()}")
            logReader(reader)
            null
        }
    }

    private fun logReader(reader: JsonReader){
        val log = JsonParser.parseReader(reader)
        println("NodeToLaTeXTypeAdapter: ")
        println(gson.toJson(log))
    }
}

