package com.phtomath.data.adapter.data.serialization

import com.google.gson.*
import com.phtomath.data.adapter.data.model.Preview
import java.lang.reflect.Type

class PreviewSerializer: JsonSerializer<Preview?> {
    override fun serialize(src: Preview?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
        return try {
            when(src){
                null -> JsonNull.INSTANCE
//                is VerticalPreview -> {
//                    val gson = Gson()
//                    val element = gson.toJsonTree(src, VerticalPreview::class.java)
//                    element.asJsonObject.remove("type")
//                    element
//                }
                else -> JsonNull.INSTANCE
            }
        }catch (e: Exception){
            println(e)
            JsonNull.INSTANCE
        }
    }
}