package com.phtomath.data.adapter.data.serialization

import com.google.gson.*
import com.phtomath.data.adapter.data.model.*
import com.phtomath.data.adapter.safeMessage
import io.ktor.util.logging.*
import java.lang.reflect.Type

class ResultGroupsDeserializer(private val logger: Logger) : JsonDeserializer<CoreGroup?>, JsonSerializer<CoreGroup?> {
    private val gson = GsonBuilder()
        .registerTypeAdapter(CoreNode::class.java, CoreNodeJsonSerializer)
        .create()

    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): CoreGroup? {
        return try {
            val group = json?.let { gson.fromJson(it, CoreGroup::class.java) }
            group?.let { coreGroup ->
                when(coreGroup.type){
                    GroupType.VERTICAL -> json.let { gson.fromJson(it, VerticalCoreGroup::class.java) }
                    else -> null
                }
            }
        }catch (e: Exception){
            logger.error(LOG_TAG, e.safeMessage())
            null
        }
    }

    override fun serialize(src: CoreGroup?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
        return try {
            when(src){
                null -> JsonNull.INSTANCE
                is VerticalCoreGroup -> {
                    val gson = GsonBuilder()
                        .registerTypeAdapter(VerticalPreview::class.java, PreviewSerializer())
                        .disableHtmlEscaping()
                        .create()
                    gson.toJsonTree(src, VerticalCoreGroup::class.java).also {
                        it.asJsonObject.remove("type")
                    }
                }
                else -> JsonNull.INSTANCE
            }
        }catch (e: Exception){
            logger.error(LOG_TAG, e.safeMessage())
            JsonNull.INSTANCE
        }
    }

    companion object{
        private const val LOG_TAG = "ResultGroupsDeserializer: "
    }
}