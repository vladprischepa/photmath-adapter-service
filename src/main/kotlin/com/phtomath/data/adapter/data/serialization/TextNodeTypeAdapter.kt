package com.phtomath.data.adapter.data.serialization

import com.google.gson.GsonBuilder
import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter
import com.phtomath.data.adapter.data.latex.TextNodeConverter
import com.phtomath.data.adapter.data.model.CoreNode
import com.phtomath.data.adapter.data.model.TextNode

class TextNodeTypeAdapter : TypeAdapter<String?>() {
    private val gson = GsonBuilder()
        .registerTypeAdapter(CoreNode::class.java, CoreNodeJsonSerializer)
        .create()
    override fun write(out: JsonWriter?, value: String?) {
        out?.let { writer ->
            value?.let { writer.value(it) }
        }
    }

    override fun read(reader: JsonReader?): String? {
        return try {
            when(reader?.peek()){
                null, JsonToken.NULL -> {
                    reader?.nextNull()
                    null
                }
                JsonToken.BEGIN_OBJECT -> {
                    val preview = gson.fromJson<TextNode>(reader, TextNode::class.java)
                    preview.let { TextNodeConverter.textNodeToString(it) }
                }
                else -> null
            }
        }catch (e: Exception){
            e.printStackTrace()
            null
        }
    }
}