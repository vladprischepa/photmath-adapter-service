package com.phtomath.data.adapter.data.serialization

import com.google.gson.JsonElement
import com.phtomath.data.adapter.data.model.*
import com.phtomath.data.adapter.data.serialization.base.Deserializer
import com.phtomath.data.adapter.data.serialization.base.IDeserializationProvider
import com.phtomath.data.adapter.data.serialization.base.ISerializationProvider
import com.phtomath.data.adapter.data.serialization.base.Serializer
import com.phtomath.data.adapter.data.serialization.deserializers.*
import com.phtomath.data.adapter.data.serialization.serializers.*
import com.phtomath.data.adapter.plugins.Analytics
import org.slf4j.Logger
import java.lang.reflect.Type

@Suppress("UNCHECKED_CAST")
class TypeFactory(
    logger: Logger,
    analytics: Analytics
): IDeserializationProvider, ISerializationProvider {

    private val deserializers: HashMap<Type, Deserializer<*>> = hashMapOf(
        CoreNode::class.java to CoreNodeDeserializer(logger, analytics, this),
        Action::class.java to ActionDeserializer(logger, analytics, this),
        CommandNode::class.java to CommandNodeDeserializer(logger, analytics, this),
        NodeAction::class.java to NodeActionDeserializer(logger, analytics, this),
        Translation::class.java to TranslationDeserializer(logger, analytics, this),
        ImageToMathInfo::class.java to ImageToMathDeserializer(logger, analytics, this),
        SolverInfo::class.java to SolverInfoDeserializer(logger, analytics, this),
        Info::class.java to InfoDeserializer(logger, analytics, this),
        TextNode::class.java to TextNodeDeserializer(logger, analytics, this),
        PreviewContent::class.java to PreviewContentDeserializer(logger, analytics, this),
        CoreSolverSubstep::class.java to CoreSolverSubstepDeserializer(logger, analytics, this),
        CoreSolverStep::class.java to CoreSolverStepDeserializer(logger, analytics, this),
        StepsResult::class.java to StepsResultDeserializer(logger, analytics, this),
        VerticalPreview::class.java to VerticalPreviewDeserializer(logger, analytics, this),
        VerticalEntry::class.java to VerticalEntryDeserializer(logger, analytics, this),
        VerticalCoreGroup::class.java to VerticalCoreGroupDeserializer(logger, analytics, this),
        Result::class.java to ResultDeserializer(logger, analytics, this),
        ImageRecognizeResponse::class.java to ImageRecognizeResponseDeserializer(logger, analytics, this),
        StepsResponse::class.java to StepsResponseDeserializer(logger, analytics, this),
        WritingTypeClassifier::class.java to WritingTypeClassifierDeserializer(logger, analytics, this)
    )
    private val serializers: HashMap<Pair<Type, Type>, Serializer<*, *>> = hashMapOf(
        Pair(CoreNode::class.java, String::class.java) to CoreNodeToStringSerializer(logger, analytics, this),
        Pair(CoreNode::class.java, JsonElement::class.java) to CoreNodeSerializer(logger, analytics, this),
        Pair(Action::class.java, JsonElement::class.java) to ActionSerializer(logger, analytics, this),
        Pair(CommandNode::class.java, JsonElement::class.java) to CommandNodeSerializer(logger, analytics, this),
        Pair(NodeAction::class.java, String::class.java) to NodeActionToStringSerializer(logger, analytics, this),
        Pair(Translation::class.java, JsonElement::class.java) to TranslationSerializer(logger, analytics, this),
        Pair(ImageToMathInfo::class.java, JsonElement::class.java) to ImageToMathSerializer(logger, analytics, this),
        Pair(SolverInfo::class.java, JsonElement::class.java) to SolverInfoSerializer(logger, analytics, this),
        Pair(Info::class.java, JsonElement::class.java) to InfoSerializer(logger, analytics, this),
        Pair(TextNode::class.java, String::class.java) to TextNodeToStringSerializer(logger, analytics, this),
        Pair(PreviewContent::class.java, JsonElement::class.java) to PreviewContentSerializer(logger, analytics, this),
        Pair(CoreSolverSubstep::class.java, JsonElement::class.java) to CoreSolverSubstepSerializer(logger, analytics, this),
        Pair(CoreSolverStep::class.java, JsonElement::class.java) to CoreSolverStepSerializer(logger, analytics, this),
        Pair(StepsResult::class.java, JsonElement::class.java) to StepsResultSerializer(logger, analytics, this),
        Pair(VerticalPreview::class.java, JsonElement::class.java) to VerticalPreviewSerializer(logger, analytics, this),
        Pair(VerticalEntry::class.java, JsonElement::class.java) to VerticalEntrySerializer(logger, analytics, this),
        Pair(VerticalCoreGroup::class.java, JsonElement::class.java) to VerticalCoreGroupSerializer(logger, analytics, this),
        Pair(Result::class.java, JsonElement::class.java) to ResultSerializer(logger, analytics, this),
        Pair(ImageRecognizeResponse::class.java, JsonElement::class.java) to ImageRecognizeResponseSerializer(logger, analytics, this),
        Pair(StepsResponse::class.java, JsonElement::class.java) to StepsResponseSerializer(logger, analytics, this),
        Pair(WritingTypeClassifier::class.java, JsonElement::class.java) to WritingTypeClassifierSerializer(logger, analytics, this)
    )

    override fun <T> deserializer(classOfT: Class<T>): Deserializer<T> {
        val deserializer = deserializers[classOfT]
        return requireNotNull(deserializer as Deserializer<T>)
    }

    override fun <T, R> serializer(classOfSource: Class<T>, classOfDest: Class<R>): Serializer<T, R?> {
        val serializer = serializers[Pair(classOfSource, classOfDest)]
        return requireNotNull(serializer as Serializer<T, R?>)
    }
}