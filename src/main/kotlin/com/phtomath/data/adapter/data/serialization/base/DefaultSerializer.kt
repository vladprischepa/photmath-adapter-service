package com.phtomath.data.adapter.data.serialization.base

import com.google.gson.Gson
import com.google.gson.JsonElement
import com.phtomath.data.adapter.safeMessage
import org.slf4j.Logger
import java.lang.reflect.Type

object DefaultSerializer {
    const val LOG_TAG: String = "DefaultSerializer:"
    private val gson = Gson()
    fun <T> serialize(logger: Logger, requestUUID: String?, src: T, srcType: Type): JsonElement? {
        return try {
            gson.toJsonTree(src, srcType)
        } catch (e: Exception) {
            logger.error("$requestUUID:$LOG_TAG${Serializer.TAG_SERIALIZE}${e.safeMessage()}")
            null
        }
    }

    fun <T> deserialize(
        logger: Logger,
        requestUUID: String?,
        json: JsonElement,
        destType: Type
    ): T? {
        return try {
            gson.fromJson(json, destType)
        } catch (e: Exception) {
            logger.error("$requestUUID:$LOG_TAG${Deserializer.TAG_DESERIALIZE}${e.safeMessage()}")
            null
        }
    }

}