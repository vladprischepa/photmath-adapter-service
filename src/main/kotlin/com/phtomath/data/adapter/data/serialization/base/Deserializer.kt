package com.phtomath.data.adapter.data.serialization.base

import com.google.gson.JsonElement
import com.phtomath.data.adapter.data.model.RequestType
import com.phtomath.data.adapter.plugins.Analytics
import org.slf4j.Logger


@Suppress("PropertyName")
public abstract class Deserializer<T>(
    protected val logger: Logger,
    protected val analytics: Analytics,
    protected val factory: IDeserializationProvider
) {
    open val LOG_TAG: String get() = "${this::class.java}:"
    companion object{
        const val TAG_DESERIALIZE = "deserialize:"
    }
    abstract fun deserialize(requestType: RequestType,
                    requestUUID: String?,
                    ticketId: String?,
                    originalImageUrl: String?,
                    json: JsonElement): T?
}