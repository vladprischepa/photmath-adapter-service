package com.phtomath.data.adapter.data.serialization.base

import java.lang.reflect.Type
import kotlin.reflect.KClass

interface IDeserializationProvider {
    fun <T> deserializer(classOfT: Class<T>): Deserializer<T>
}