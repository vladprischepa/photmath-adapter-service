package com.phtomath.data.adapter.data.serialization.base

interface ISerializationProvider {
    fun <T, R> serializer(classOfSource: Class<T>, classOfDest: Class<R>): Serializer<T,R?>
}