package com.phtomath.data.adapter.data.serialization.base

import com.phtomath.data.adapter.plugins.Analytics
import org.slf4j.Logger

@Suppress("PropertyName")
abstract class Serializer<T, R>(
    val logger: Logger,
    val analytics: Analytics,
    val factory: ISerializationProvider
) {
    open val LOG_TAG: String get() = "${this::class.java.simpleName}:"
    companion object{
        const val TAG_SERIALIZE = "serialize:"
    }
    abstract fun serialize(requestUUID: String?, src: T): R
}