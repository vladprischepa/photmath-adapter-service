package com.phtomath.data.adapter.data.serialization.deserializers

import com.google.gson.JsonElement
import com.phtomath.data.adapter.data.model.Action
import com.phtomath.data.adapter.data.model.CoreNode
import com.phtomath.data.adapter.data.model.RequestType
import com.phtomath.data.adapter.data.serialization.base.Deserializer
import com.phtomath.data.adapter.data.serialization.base.IDeserializationProvider
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeArg
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class ActionDeserializer(
    logger: Logger,
    analytics: Analytics,
    deserializationProvider: IDeserializationProvider
): Deserializer<Action?>(logger, analytics, deserializationProvider) {

    override fun deserialize(
        requestType: RequestType,
        requestUUID: String?,
        ticketId: String?,
        originalImageUrl: String?,
        json: JsonElement
    ): Action? {
        return try {
            val coreNodeDeserializer = factory.deserializer(CoreNode::class.java)
            val command = json.asJsonObject?.getAsJsonPrimitive(SerializationKeys.KEY_COMMAND)?.asString
            val args = json.asJsonObject?.getAsJsonArray(SerializationKeys.KEY_ARGS)?.mapNotNull { node ->
                coreNodeDeserializer.deserialize(requestType, requestUUID, ticketId, originalImageUrl, node)
            }.safeArg(emptyList())
            Action(
                command = command,
                args = args
            )
        }catch (e: Exception){
            logger.error("$requestUUID:$LOG_TAG$TAG_DESERIALIZE${e.safeMessage()}")
            null
        }
    }
}