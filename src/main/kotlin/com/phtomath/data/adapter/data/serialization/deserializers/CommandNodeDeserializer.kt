package com.phtomath.data.adapter.data.serialization.deserializers

import com.google.gson.JsonElement
import com.phtomath.data.adapter.data.model.Action
import com.phtomath.data.adapter.data.model.CommandNode
import com.phtomath.data.adapter.data.model.CoreNode
import com.phtomath.data.adapter.data.model.RequestType
import com.phtomath.data.adapter.data.serialization.base.Deserializer
import com.phtomath.data.adapter.data.serialization.base.IDeserializationProvider
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class CommandNodeDeserializer(
    logger: Logger,
    analytics: Analytics,
    factory: IDeserializationProvider
    ): Deserializer<CommandNode?>(logger, analytics, factory) {
    override  fun deserialize(
        requestType: RequestType,
        requestUUID: String?,
        ticketId: String?,
        originalImageUrl: String?,
        json: JsonElement
    ): CommandNode? {
        return try {
            val node = json.asJsonObject?.get(SerializationKeys.KEY_NODE)?.let { element ->
                factory.deserializer(CoreNode::class.java)
                    .deserialize(requestType, requestUUID, ticketId, originalImageUrl, element)
            }
            val action = json.asJsonObject?.get(SerializationKeys.KEY_ACTION)?.let {  element ->
                factory.deserializer(Action::class.java).deserialize(requestType, requestUUID, ticketId, originalImageUrl, element)
            }
            CommandNode(action, node)
        }catch (e: Exception){
            logger.error("$requestUUID:$LOG_TAG${TAG_DESERIALIZE}${e.safeMessage()}")
            null
        }
    }
}