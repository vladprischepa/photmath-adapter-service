package com.phtomath.data.adapter.data.serialization.deserializers

import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.phtomath.data.adapter.data.model.CoreNode
import com.phtomath.data.adapter.data.model.NodeType
import com.phtomath.data.adapter.data.model.RequestType
import com.phtomath.data.adapter.data.serialization.base.Deserializer
import com.phtomath.data.adapter.data.serialization.base.IDeserializationProvider
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.plugins.takeIfInstance
import com.phtomath.data.adapter.safeArg
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class CoreNodeDeserializer(
    logger: Logger,
    analytics: Analytics,
    deserializerProvider: IDeserializationProvider
) : Deserializer<CoreNode>(logger, analytics, deserializerProvider) {

    companion object{
//        private const val LOG_TAG = "CoreNodeDeserializer: "
        private const val SUB_TAG_PARSE_NODE = "parseNode:"
    }
    private val gson = Gson()

    override fun deserialize(
        requestType: RequestType,
        requestUUID: String?,
        ticketId: String?,
        originalImageUrl: String?,
        json: JsonElement
    ): CoreNode {
        return parseNode(requestType, requestUUID, ticketId, originalImageUrl, json)
    }

    private fun parseNode(requestType: RequestType,
                                  requestUUID: String?,
                                  ticketId: String?,
                                  originalImageUrl: String?,
                                  element: JsonElement): CoreNode {
        return try {
            element.asJsonObject?.let { obj ->
                val type: NodeType = parseType(requestType, requestUUID, ticketId, originalImageUrl, obj)
                val value: String? = obj.getAsJsonPrimitive(SerializationKeys.KEY_VALUE)?.asString
                val color: Int? = obj.getAsJsonPrimitive(SerializationKeys.KEY_COLOR)?.asInt
                val children: List<CoreNode> = obj.getAsJsonArray(SerializationKeys.KEY_CHILDREN)
                    ?.map { parseNode(requestType, requestUUID, ticketId, originalImageUrl, it) }
                    .safeArg(emptyList())
                CoreNode(
                    children,
                    type,
                    value,
                    color
                )
            }?: parseErrorNode(requestType, requestUUID, ticketId, originalImageUrl, element)
        }catch (e: Exception){
            logger.error("$requestUUID:$LOG_TAG$SUB_TAG_PARSE_NODE", e.safeMessage())
            parseErrorNode(requestType, requestUUID, ticketId, originalImageUrl, element)
        }
    }

    private fun parseType(
        requestType: RequestType,
        requestUUID: String?,
        ticketId: String?,
        originalImageUrl: String?,
        obj: JsonObject): NodeType {
        val type =  obj.getAsJsonPrimitive(SerializationKeys.KEY_TYPE)
            ?.asString
            ?.let { NodeType.parse(it) }
            .safeArg(NodeType.Undefined)
        type.takeIfInstance(NodeType.Unknown::class.java)?.apply {
            nodeJsonString = gson.toJson(obj)
            logger.error("Unknown Node: $key")
            logger.error("Unknown Node: $nodeJsonString")
            analytics.sendUnrecognizedCoreNodeEvent(
                this, requestType, requestUUID, ticketId, originalImageUrl
            )
        }
        return type
    }

    private fun parseErrorNode(
        requestType: RequestType,
        requestUUID: String?,
        ticketId: String?,
        originalImageUrl: String?,
        element: JsonElement?): CoreNode {
        val type: NodeType = element?.asJsonObject
            ?.let { parseType(requestType, requestUUID, ticketId, originalImageUrl, it) }
            .safeArg(NodeType.Undefined)
        val value = element?.asJsonObject?.getAsJsonPrimitive(SerializationKeys.KEY_VALUE)?.asString
        return CoreNode(null, type = type, value = value, color = null)
    }
}