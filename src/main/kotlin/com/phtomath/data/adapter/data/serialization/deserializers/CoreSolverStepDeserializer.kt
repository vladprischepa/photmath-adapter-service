package com.phtomath.data.adapter.data.serialization.deserializers

import com.google.gson.JsonElement
import com.phtomath.data.adapter.data.model.CoreSolverStep
import com.phtomath.data.adapter.data.model.CoreSolverSubstep
import com.phtomath.data.adapter.data.model.RequestType
import com.phtomath.data.adapter.data.model.TextNode
import com.phtomath.data.adapter.data.serialization.base.Deserializer
import com.phtomath.data.adapter.data.serialization.base.IDeserializationProvider
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class CoreSolverStepDeserializer(
    logger: Logger,
    analytics: Analytics,
    factory: IDeserializationProvider
): Deserializer<CoreSolverStep?>(logger, analytics, factory) {
    override fun deserialize(
        requestType: RequestType,
        requestUUID: String?,
        ticketId: String?,
        originalImageUrl: String?,
        json: JsonElement
    ): CoreSolverStep? {
        return try {
            val textNodeDeserializer = factory.deserializer(TextNode::class.java)
            val substepDeserializer = factory.deserializer(CoreSolverSubstep::class.java)
            json.asJsonObject?.let { obj ->
                val headers = obj.getAsJsonArray(SerializationKeys.KEY_HEADERS)?.mapNotNull {
                    textNodeDeserializer.deserialize(requestType, requestUUID, ticketId, originalImageUrl, it)
                }
                val substeps = obj.getAsJsonArray(SerializationKeys.KEY_SUBSTEPS)?.mapNotNull {
                    substepDeserializer.deserialize(requestType, requestUUID, ticketId, originalImageUrl, it)
                }
                CoreSolverStep(headers, substeps)
            }
        }catch (e: Exception){
            logger.error("$requestUUID:${LOG_TAG}$TAG_DESERIALIZE", e.safeMessage())
            null
        }
    }
}