package com.phtomath.data.adapter.data.serialization.deserializers

import com.google.gson.JsonElement
import com.phtomath.data.adapter.data.model.*
import com.phtomath.data.adapter.data.serialization.base.Deserializer
import com.phtomath.data.adapter.data.serialization.base.IDeserializationProvider
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class CoreSolverSubstepDeserializer(
    logger: Logger,
    analytics: Analytics,
    deserializationProvider: IDeserializationProvider
): Deserializer<CoreSolverSubstep?>(logger, analytics, deserializationProvider) {
    override fun deserialize(
        requestType: RequestType,
        requestUUID: String?,
        ticketId: String?,
        originalImageUrl: String?,
        json: JsonElement
    ): CoreSolverSubstep? {
        return try {
            val textNodeDeserializer = factory.deserializer(TextNode::class.java)
            val coreNodeDeserializer = factory.deserializer(CoreNode::class.java)
            val stepsResultDeserializer = factory.deserializer(StepsResult::class.java)
            json.asJsonObject?.let { obj ->
                val desc = obj.get(SerializationKeys.KEY_DESCRIPTION)?.let {
                    textNodeDeserializer.deserialize(requestType, requestUUID, ticketId, originalImageUrl, it)
                }
                val left = obj.get(SerializationKeys.KEY_LEFT)?.let {
                    coreNodeDeserializer.deserialize(requestType, requestUUID, ticketId, originalImageUrl, it)
                }
                val right = obj.get(SerializationKeys.KEY_RIGHT)?.let {
                    coreNodeDeserializer.deserialize(requestType, requestUUID, ticketId, originalImageUrl, it)
                }
                val result = obj.get(SerializationKeys.KEY_SUBRESULT)?.let {
                    stepsResultDeserializer.deserialize(requestType, requestUUID, ticketId, originalImageUrl, it)
                }
                CoreSolverSubstep(
                    desc, left, right, result
                )
            }
        }catch (e: Exception){
            logger.error("$requestUUID:${LOG_TAG}$TAG_DESERIALIZE", e.safeMessage())
            null
        }
    }

}