package com.phtomath.data.adapter.data.serialization.deserializers

import com.google.gson.JsonElement
import com.phtomath.data.adapter.data.model.ImageRecognizeResponse
import com.phtomath.data.adapter.data.model.Info
import com.phtomath.data.adapter.data.model.RequestType
import com.phtomath.data.adapter.data.model.Result
import com.phtomath.data.adapter.data.serialization.base.Deserializer
import com.phtomath.data.adapter.data.serialization.base.IDeserializationProvider
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class ImageRecognizeResponseDeserializer(
    logger: Logger,
    analytics: Analytics,
    factory: IDeserializationProvider
): Deserializer<ImageRecognizeResponse?>(logger, analytics, factory) {
    override fun deserialize(
        requestType: RequestType,
        requestUUID: String?,
        ticketId: String?,
        originalImageUrl: String?,
        json: JsonElement
    ): ImageRecognizeResponse? {
        return try {
            val resultDeserializer = factory.deserializer(Result::class.java)
            val infoDeserializer = factory.deserializer(Info::class.java)
            json.asJsonObject?.let { obj ->
                val result = obj.getAsJsonObject(SerializationKeys.KEY_RESULT)?.let {
                    resultDeserializer.deserialize(requestType, requestUUID, ticketId, originalImageUrl, it)
                }
                val info = obj.getAsJsonObject(SerializationKeys.KEY_INFO)?.let {
                    infoDeserializer.deserialize(requestType, requestUUID, ticketId, originalImageUrl, it)
                }
                ImageRecognizeResponse(
                    result, info
                )
            }
        }catch (e: Exception){
            logger.error("$requestUUID:${LOG_TAG}$TAG_DESERIALIZE${e.safeMessage()}")
            null
        }
    }
}