package com.phtomath.data.adapter.data.serialization.deserializers

import com.google.gson.JsonElement
import com.phtomath.data.adapter.data.model.CommandNode
import com.phtomath.data.adapter.data.model.ImageToMathInfo
import com.phtomath.data.adapter.data.model.RequestType
import com.phtomath.data.adapter.data.serialization.base.Deserializer
import com.phtomath.data.adapter.data.serialization.base.IDeserializationProvider
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class ImageToMathDeserializer(
    logger: Logger,
    analytics: Analytics,
    factory: IDeserializationProvider
) : Deserializer<ImageToMathInfo?>(logger, analytics, factory) {

    override fun deserialize(
        requestType: RequestType,
        requestUUID: String?,
        ticketId: String?,
        originalImageUrl: String?,
        json: JsonElement
    ): ImageToMathInfo? {
        return try {
            json.asJsonObject?.let { obj ->
                val contentType = obj.getAsJsonPrimitive(SerializationKeys.KEY_CONTENT_TYPE)?.asString
                val grainedContentType = obj.getAsJsonPrimitive(SerializationKeys.KEY_GRAINED_CONTENT_TYPE)?.asString
                val version = obj.getAsJsonPrimitive(SerializationKeys.KEY_VERSION)?.asString
                val command = obj.get(SerializationKeys.KEY_COMMAND)?.let { commandNode ->
                    factory.deserializer(CommandNode::class.java)
                        .deserialize(requestType, requestUUID, ticketId, originalImageUrl, commandNode)
                }
                ImageToMathInfo(command, contentType, version, grainedContentType)
            }
        } catch (e: Exception) {
            logger.error("$requestUUID:$LOG_TAG$TAG_DESERIALIZE${e.safeMessage()}")
            null
        }
    }
}