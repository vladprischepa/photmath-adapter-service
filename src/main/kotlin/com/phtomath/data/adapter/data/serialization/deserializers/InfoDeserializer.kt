package com.phtomath.data.adapter.data.serialization.deserializers

import com.google.gson.JsonElement
import com.phtomath.data.adapter.data.model.*
import com.phtomath.data.adapter.data.serialization.base.DefaultSerializer
import com.phtomath.data.adapter.data.serialization.base.Deserializer
import com.phtomath.data.adapter.data.serialization.base.IDeserializationProvider
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class InfoDeserializer(
    logger: Logger,
    analytics: Analytics,
    factory: IDeserializationProvider
): Deserializer<Info?>(logger, analytics, factory) {

    override fun deserialize(
        requestType: RequestType,
        requestUUID: String?,
        ticketId: String?,
        originalImageUrl: String?,
        json: JsonElement
    ): Info? {
        return try {
            json.asJsonObject?.let { obj ->
                val imageToMath = obj.get(SerializationKeys.KEY_IMAGE_TO_MATH)
                    ?.let { imageToMath ->
                        factory.deserializer(ImageToMathInfo::class.java)
                            .deserialize(requestType, requestUUID, ticketId, originalImageUrl, imageToMath)
                    }
                val solverInfo = obj.get(SerializationKeys.KEY_SOLVER)
                    ?.let { solverInfo ->
                        factory.deserializer(SolverInfo::class.java)
                            .deserialize(requestType, requestUUID, ticketId, originalImageUrl, solverInfo)
                    }
                val writingTypeClassifier = obj.get(SerializationKeys.KEY_WRITING_CLASSIFIER)
                    ?.let { classifier ->
                        DefaultSerializer.deserialize<WritingTypeClassifier>(logger, requestUUID, classifier, WritingTypeClassifier::class.java)
//                        factory.deserializer(WritingTypeClassifier::class.java)
//                            .deserialize(requestType, requestUUID, ticketId, originalImageUrl, classifier)
                    }
                Info(solverInfo, imageToMath, writingTypeClassifier)
            }
        }catch (e: Exception){
            logger.error("$requestUUID:$LOG_TAG$TAG_DESERIALIZE${e.safeMessage()}")
            null
        }
    }
}