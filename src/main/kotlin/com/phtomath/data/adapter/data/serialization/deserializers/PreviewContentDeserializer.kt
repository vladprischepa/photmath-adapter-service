package com.phtomath.data.adapter.data.serialization.deserializers

import com.google.gson.JsonElement
import com.phtomath.data.adapter.data.model.CoreNode
import com.phtomath.data.adapter.data.model.PreviewContent
import com.phtomath.data.adapter.data.model.RequestType
import com.phtomath.data.adapter.data.model.TextNode
import com.phtomath.data.adapter.data.serialization.base.Deserializer
import com.phtomath.data.adapter.data.serialization.base.IDeserializationProvider
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class PreviewContentDeserializer(
    logger: Logger,
    analytics: Analytics,
    deserializationProvider: IDeserializationProvider
): Deserializer<PreviewContent?>(logger, analytics, deserializationProvider) {
    override fun deserialize(
        requestType: RequestType,
        requestUUID: String?,
        ticketId: String?,
        originalImageUrl: String?,
        json: JsonElement
    ): PreviewContent? {
        return try {
            json.asJsonObject?.let { obj ->
                val textNodeDeserializer = factory.deserializer(TextNode::class.java)
                val coreNodeDeserializer = factory.deserializer(CoreNode::class.java)
                val desc: TextNode? = obj.getAsJsonObject(SerializationKeys.KEY_DESCRIPTION)
                    ?.let { textNodeDeserializer.deserialize(requestType, requestUUID, ticketId, originalImageUrl, it) }
                val problem: CoreNode? = obj.getAsJsonObject(SerializationKeys.KEY_PROBLEM)
                    ?.let { coreNodeDeserializer.deserialize(requestType, requestUUID, ticketId, originalImageUrl, it) }
                val solution: CoreNode? = obj.getAsJsonObject(SerializationKeys.KEY_SOLUTION)
                    ?.let { coreNodeDeserializer.deserialize(requestType, requestUUID, ticketId, originalImageUrl, it) }
                PreviewContent(
                    desc, problem, solution
                )
            }
        }catch (e: Exception){
            logger.error("$requestUUID:${LOG_TAG}$TAG_DESERIALIZE${e.safeMessage()}")
            null
        }
    }
}