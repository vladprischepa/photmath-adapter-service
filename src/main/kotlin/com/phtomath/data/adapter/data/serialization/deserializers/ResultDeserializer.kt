package com.phtomath.data.adapter.data.serialization.deserializers

import com.google.gson.JsonElement
import com.phtomath.data.adapter.data.model.GroupType
import com.phtomath.data.adapter.data.model.RequestType
import com.phtomath.data.adapter.data.model.Result
import com.phtomath.data.adapter.data.model.VerticalCoreGroup
import com.phtomath.data.adapter.data.serialization.base.Deserializer
import com.phtomath.data.adapter.data.serialization.base.IDeserializationProvider
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeArg
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class ResultDeserializer(
    logger: Logger,
    analytics: Analytics,
    factory: IDeserializationProvider
): Deserializer<Result?>(logger, analytics, factory) {
    override fun deserialize(
        requestType: RequestType,
        requestUUID: String?,
        ticketId: String?,
        originalImageUrl: String?,
        json: JsonElement
    ): Result? {
        return  try {
            val verticalCoreGroupDeserializer = factory.deserializer(VerticalCoreGroup::class.java)
            json.asJsonObject?.let { obj ->
                val groups: List<VerticalCoreGroup> = obj.getAsJsonArray(SerializationKeys.KEY_GROUPS)
                    ?.filter { it.asJsonObject.getAsJsonPrimitive(SerializationKeys.KEY_TYPE).asString == GroupType.VERTICAL.key }
                    ?.mapNotNull {
                        verticalCoreGroupDeserializer.deserialize(requestType, requestUUID, ticketId, originalImageUrl, it)
                    }.safeArg(emptyList())
                Result(groups)
            }
        }catch (e: Exception){
            logger.error("$requestUUID:${LOG_TAG}$TAG_DESERIALIZE${e.safeMessage()}")
            null
        }
    }
}