package com.phtomath.data.adapter.data.serialization.deserializers

import com.google.gson.JsonElement
import com.phtomath.data.adapter.data.model.CommandNode
import com.phtomath.data.adapter.data.model.RequestType
import com.phtomath.data.adapter.data.model.SolverInfo
import com.phtomath.data.adapter.data.serialization.base.Deserializer
import com.phtomath.data.adapter.data.serialization.base.IDeserializationProvider
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class SolverInfoDeserializer(
    logger: Logger,
    analytics: Analytics,
    factory: IDeserializationProvider
) : Deserializer<SolverInfo?>(logger, analytics, factory) {

    override fun deserialize(
        requestType: RequestType,
        requestUUID: String?,
        ticketId: String?,
        originalImageUrl: String?,
        json: JsonElement
    ): SolverInfo? {
        return try {
            json.asJsonObject?.let { obj ->
                val errorType = obj.getAsJsonPrimitive(SerializationKeys.KEY_ERROR_TYPE)?.asString
                val ioVersion = obj.getAsJsonPrimitive(SerializationKeys.KEY_IO_VERSION)?.asString
                val version = obj.getAsJsonPrimitive(SerializationKeys.KEY_VERSION)?.asString
                val normalizedInput = obj.get(SerializationKeys.KEY_NORMALIZED_INPUT)?.let { node ->
                    factory.deserializer(CommandNode::class.java)
                        .deserialize(requestType, requestUUID, ticketId, originalImageUrl, node)
                }
                SolverInfo(errorType, ioVersion, normalizedInput, version)
            }
        } catch (e: Exception) {
            logger.error("$requestUUID:$LOG_TAG$TAG_DESERIALIZE${e.safeMessage()}")
            null
        }
    }
}