package com.phtomath.data.adapter.data.serialization.deserializers

import com.google.gson.JsonElement
import com.phtomath.data.adapter.data.model.Info
import com.phtomath.data.adapter.data.model.RequestType
import com.phtomath.data.adapter.data.model.StepsResponse
import com.phtomath.data.adapter.data.model.StepsResult
import com.phtomath.data.adapter.data.serialization.base.Deserializer
import com.phtomath.data.adapter.data.serialization.base.IDeserializationProvider
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class StepsResponseDeserializer(
    logger: Logger,
    analytics: Analytics,
    factory: IDeserializationProvider
): Deserializer<StepsResponse?>(logger, analytics, factory) {
    override fun deserialize(
        requestType: RequestType,
        requestUUID: String?,
        ticketId: String?,
        originalImageUrl: String?,
        json: JsonElement
    ): StepsResponse? {
        return try {
            val stepsResultDeserializer = factory.deserializer(StepsResult::class.java)
            val infoDeserializer = factory.deserializer(Info::class.java)
            json.asJsonObject?.let { obj ->
                val result = obj.get(SerializationKeys.KEY_RESULT)?.let {
                    stepsResultDeserializer.deserialize(requestType, requestUUID, ticketId, originalImageUrl, it)
                }
                val info = obj.get(SerializationKeys.KEY_INFO)?.let {
                    infoDeserializer.deserialize(requestType, requestUUID, ticketId, originalImageUrl, it)
                }
                StepsResponse(result, info)
            }
        }catch (e: Exception){
            logger.error("$requestUUID:${LOG_TAG}${Deserializer.TAG_DESERIALIZE}", e.safeMessage())
            null
        }
    }
}