package com.phtomath.data.adapter.data.serialization.deserializers

import com.google.gson.JsonElement
import com.phtomath.data.adapter.data.model.*
import com.phtomath.data.adapter.data.serialization.base.Deserializer
import com.phtomath.data.adapter.data.serialization.base.IDeserializationProvider
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class StepsResultDeserializer(
    logger: Logger,
    analytics: Analytics,
    provider: IDeserializationProvider
): Deserializer<StepsResult?>(logger, analytics, provider) {
    override fun deserialize(
        requestType: RequestType,
        requestUUID: String?,
        ticketId: String?,
        originalImageUrl: String?,
        json: JsonElement
    ): StepsResult? {
        return try {
            val textNodeDeserializer = factory.deserializer(TextNode::class.java)
            val coreNodeDeserializer = factory.deserializer(CoreNode::class.java)
            val coreSolverStepDeserializer = factory.deserializer(CoreSolverStep::class.java)
            json.asJsonObject?.let { obj ->
                val header = obj.get(SerializationKeys.KEY_HEADER)?.let {
                    textNodeDeserializer.deserialize(requestType, requestUUID, ticketId, originalImageUrl, it)
                }
                val solution = obj.get(SerializationKeys.KEY_SOLUTION)?.let {
                    coreNodeDeserializer.deserialize(requestType, requestUUID, ticketId, originalImageUrl, it)
                }
                val steps = obj.getAsJsonArray(SerializationKeys.KEY_STEPS)?.mapNotNull {
                    coreSolverStepDeserializer.deserialize(requestType, requestUUID, ticketId, originalImageUrl, it)
                }
                StepsResult(
                    header, solution, steps
                )
            }
        }catch (e: Exception){
            e.printStackTrace()
            logger.error("$requestUUID:${LOG_TAG}$TAG_DESERIALIZE", e.safeMessage())
            null
        }
    }
}