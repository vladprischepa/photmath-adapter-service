package com.phtomath.data.adapter.data.serialization.deserializers

import com.google.gson.JsonElement
import com.phtomath.data.adapter.data.model.CoreNode
import com.phtomath.data.adapter.data.model.RequestType
import com.phtomath.data.adapter.data.model.TextNode
import com.phtomath.data.adapter.data.model.Translation
import com.phtomath.data.adapter.data.serialization.base.Deserializer
import com.phtomath.data.adapter.data.serialization.base.IDeserializationProvider
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeArg
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class TextNodeDeserializer(
    logger: Logger,
    analytics: Analytics,
    deserializationProvider: IDeserializationProvider
) : Deserializer<TextNode?>(logger, analytics, deserializationProvider) {
    override fun deserialize(
        requestType: RequestType,
        requestUUID: String?,
        ticketId: String?,
        originalImageUrl: String?,
        json: JsonElement
    ): TextNode? {
        return try {
            json.asJsonObject?.let { obj ->
                val coreNodeDeserializer = factory.deserializer(CoreNode::class.java)
                val translationDeserializer = factory.deserializer(Translation::class.java)
                val type: String? = obj.getAsJsonPrimitive(SerializationKeys.KEY_TYPE)?.asString
                val args: List<CoreNode> = obj.getAsJsonArray(SerializationKeys.KEY_ARGS)
                    ?.mapNotNull { coreNodeDeserializer.deserialize(requestType, requestUUID, ticketId, originalImageUrl, it) }
                    .safeArg(emptyList())
                val voiceKey: String? = obj.getAsJsonPrimitive(SerializationKeys.KEY_VOICE_KEY)?.asString
                val localizedText: Translation? = obj.get(SerializationKeys.KEY_LOCALIZED_TEXT)
                    ?.let { translationDeserializer.deserialize(requestType, requestUUID, ticketId, originalImageUrl, it) }
                val localizedVoiceText: Translation? = obj.get(SerializationKeys.KEY_LOCALIZED_VOICE_TEXT)
                    ?.let { translationDeserializer.deserialize(requestType, requestUUID, ticketId, originalImageUrl, it) }
                TextNode(
                    type, args, voiceKey, localizedText, localizedVoiceText
                )
            }
        }catch (e: Exception){
            logger.error("$requestUUID:${LOG_TAG}$TAG_DESERIALIZE${e.safeMessage()}")
            null
        }
    }

}