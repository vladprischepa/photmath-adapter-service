package com.phtomath.data.adapter.data.serialization.deserializers

import com.google.gson.JsonElement
import com.phtomath.data.adapter.data.model.GroupType
import com.phtomath.data.adapter.data.model.RequestType
import com.phtomath.data.adapter.data.model.VerticalCoreGroup
import com.phtomath.data.adapter.data.model.VerticalEntry
import com.phtomath.data.adapter.data.serialization.base.Deserializer
import com.phtomath.data.adapter.data.serialization.base.IDeserializationProvider
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeArg
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class VerticalCoreGroupDeserializer(
    logger: Logger,
    analytics: Analytics,
    factory: IDeserializationProvider
): Deserializer<VerticalCoreGroup?>(logger, analytics, factory) {
    override fun deserialize(
        requestType: RequestType,
        requestUUID: String?,
        ticketId: String?,
        originalImageUrl: String?,
        json: JsonElement
    ): VerticalCoreGroup? {
        return try {
            val verticalEntryDeserializer = factory.deserializer(VerticalEntry::class.java)
            json.asJsonObject?.let { obj ->
                val groupType = obj.getAsJsonPrimitive(SerializationKeys.KEY_TYPE).asString?.let { GroupType.parse(it) }
                    .safeArg(GroupType.UNKNOWN)
                val entries: List<VerticalEntry> = obj.getAsJsonArray(SerializationKeys.KEY_ENTRIES)?.mapNotNull {
                    verticalEntryDeserializer.deserialize(requestType, requestUUID, ticketId, originalImageUrl, it)
                }.safeArg(emptyList())
                VerticalCoreGroup(
                    entries
                ).apply {
                    this.type = groupType
                }
            }
        }catch (e: Exception){
            logger.error("$requestUUID:${LOG_TAG}$TAG_DESERIALIZE${e.safeMessage()}")
            null
        }
    }
}