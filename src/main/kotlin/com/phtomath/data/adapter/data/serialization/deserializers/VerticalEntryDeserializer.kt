package com.phtomath.data.adapter.data.serialization.deserializers

import com.google.gson.JsonElement
import com.phtomath.data.adapter.data.model.NodeAction
import com.phtomath.data.adapter.data.model.RequestType
import com.phtomath.data.adapter.data.model.VerticalEntry
import com.phtomath.data.adapter.data.model.VerticalPreview
import com.phtomath.data.adapter.data.serialization.base.Deserializer
import com.phtomath.data.adapter.data.serialization.base.IDeserializationProvider
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class VerticalEntryDeserializer(
    logger: Logger,
    analytics: Analytics,
    factory: IDeserializationProvider
): Deserializer<VerticalEntry?>(logger, analytics, factory) {
    override fun deserialize(
        requestType: RequestType,
        requestUUID: String?,
        ticketId: String?,
        originalImageUrl: String?,
        json: JsonElement
    ): VerticalEntry? {
        return try {
            val nodeActionDeserializer = factory.deserializer(NodeAction::class.java)
            val previewDeserializer = factory.deserializer(VerticalPreview::class.java)
            json.asJsonObject?.let { obj ->
                val nodeAction: NodeAction? = obj.getAsJsonObject(SerializationKeys.KEY_NODE_ACTION)?.let {
                    nodeActionDeserializer.deserialize(requestType, requestUUID, ticketId, originalImageUrl, it)
                }
                val preview: VerticalPreview? = obj.getAsJsonObject(SerializationKeys.KEY_PREVIEW)?.let {
                    previewDeserializer.deserialize(requestType, requestUUID, ticketId, originalImageUrl, it)
                }
                VerticalEntry(
                    nodeAction, preview
                )
            }
        }catch (e: Exception){
            logger.error("$requestUUID:${LOG_TAG}$TAG_DESERIALIZE${e.safeMessage()}")
            null
        }
    }
}