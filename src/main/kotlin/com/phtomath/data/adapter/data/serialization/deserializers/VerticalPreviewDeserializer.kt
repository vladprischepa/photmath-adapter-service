package com.phtomath.data.adapter.data.serialization.deserializers

import com.google.gson.JsonElement
import com.phtomath.data.adapter.data.model.PreviewContent
import com.phtomath.data.adapter.data.model.RequestType
import com.phtomath.data.adapter.data.model.TextNode
import com.phtomath.data.adapter.data.model.VerticalPreview
import com.phtomath.data.adapter.data.serialization.base.Deserializer
import com.phtomath.data.adapter.data.serialization.base.IDeserializationProvider
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class VerticalPreviewDeserializer(
    logger: Logger,
    analytics: Analytics,
    factory: IDeserializationProvider
): Deserializer<VerticalPreview?>(logger, analytics, factory) {
    override fun deserialize(
        requestType: RequestType,
        requestUUID: String?,
        ticketId: String?,
        originalImageUrl: String?,
        json: JsonElement
    ): VerticalPreview? {
        return try {
            val previewContentDeserializer = factory.deserializer(PreviewContent::class.java)
            val textNodeDeserializer = factory.deserializer(TextNode::class.java)
            json.asJsonObject?.let { obj ->
                val content: PreviewContent? = obj.getAsJsonObject(SerializationKeys.KEY_CONTENT)?.let {
                    previewContentDeserializer.deserialize(requestType, requestUUID, ticketId, originalImageUrl, it)
                }
                val title: TextNode? = obj.getAsJsonObject(SerializationKeys.KEY_TITLE)?.let {
                    textNodeDeserializer.deserialize(requestType, requestUUID, ticketId, originalImageUrl, it)
                }
                val method: TextNode? = obj.getAsJsonObject(SerializationKeys.KEY_METHOD)?.let {
                    textNodeDeserializer.deserialize(requestType, requestUUID, ticketId, originalImageUrl, it)
                }
                VerticalPreview(
                    content, method, title
                )
            }
        }catch (e: Exception){
            logger.error("$requestUUID:${LOG_TAG}$TAG_DESERIALIZE${e.safeMessage()}")
            null
        }
    }
}