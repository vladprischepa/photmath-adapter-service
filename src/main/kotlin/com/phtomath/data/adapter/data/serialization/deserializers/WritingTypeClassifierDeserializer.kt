package com.phtomath.data.adapter.data.serialization.deserializers

import com.google.gson.JsonElement
import com.phtomath.data.adapter.data.model.CommandNode
import com.phtomath.data.adapter.data.model.ImageToMathInfo
import com.phtomath.data.adapter.data.model.RequestType
import com.phtomath.data.adapter.data.model.WritingTypeClassifier
import com.phtomath.data.adapter.data.serialization.base.Deserializer
import com.phtomath.data.adapter.data.serialization.base.IDeserializationProvider
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class WritingTypeClassifierDeserializer(
    logger: Logger,
    analytics: Analytics,
    factory: IDeserializationProvider
) : Deserializer<WritingTypeClassifier?>(logger, analytics, factory) {
    override fun deserialize(
        requestType: RequestType,
        requestUUID: String?,
        ticketId: String?,
        originalImageUrl: String?,
        json: JsonElement
    ): WritingTypeClassifier? {
        return try {
            json.asJsonObject?.let { obj ->
                val version = obj.getAsJsonPrimitive(SerializationKeys.KEY_VERSION)?.asString
                val type = obj.getAsJsonPrimitive(SerializationKeys.KEY_TYPE)?.asString
                val score = obj.getAsJsonPrimitive(SerializationKeys.KEY_SCORE)?.asFloat
                WritingTypeClassifier(version, type, score)
            }
        } catch (e: Exception) {
            logger.error("$requestUUID:$LOG_TAG$TAG_DESERIALIZE${e.safeMessage()}")
            null
        }
    }
}