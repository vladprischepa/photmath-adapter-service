package com.phtomath.data.adapter.data.serialization.serializers

import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.phtomath.data.adapter.data.model.Action
import com.phtomath.data.adapter.data.model.CoreNode
import com.phtomath.data.adapter.data.serialization.base.ISerializationProvider
import com.phtomath.data.adapter.data.serialization.base.Serializer
import com.phtomath.data.adapter.data.serialization.deserializers.SerializationKeys
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class ActionSerializer(
    logger: Logger,
    analytics: Analytics,
    factory: ISerializationProvider
): Serializer<Action, JsonElement?>(logger, analytics, factory) {

    override fun serialize(requestUUID: String?, src: Action): JsonElement? {
        return  try {
            val coreNodeSerializer = factory.serializer(CoreNode::class.java, JsonElement::class.java)
            JsonObject().apply {
                src.command?.also {
                    addProperty(SerializationKeys.KEY_COMMAND, it)
                }
                val array = JsonArray()
                src.args.mapNotNull { coreNodeSerializer.serialize(requestUUID, it) }.forEach { array.add(it) }
                add(SerializationKeys.KEY_ARGS, array)
            }
        }catch (e: Exception){
            logger.error("$requestUUID:${LOG_TAG}$TAG_SERIALIZE${e.safeMessage()}")
            null
        }
    }
}