package com.phtomath.data.adapter.data.serialization.serializers

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.phtomath.data.adapter.data.model.Action
import com.phtomath.data.adapter.data.model.CommandNode
import com.phtomath.data.adapter.data.model.CoreNode
import com.phtomath.data.adapter.data.serialization.base.ISerializationProvider
import com.phtomath.data.adapter.data.serialization.base.Serializer
import com.phtomath.data.adapter.data.serialization.deserializers.SerializationKeys
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class CommandNodeSerializer(
    logger: Logger,
    analytics: Analytics,
    factory: ISerializationProvider
): Serializer<CommandNode, JsonElement?>(logger, analytics, factory){

    override fun serialize(requestUUID: String?, src: CommandNode): JsonElement? {
        return try {
            JsonObject().apply {
                src.action?.let { action ->
                    factory.serializer(Action::class.java, JsonElement::class.java)
                        .serialize(requestUUID, action)
                }?.also { add(SerializationKeys.KEY_ACTION, it) }
                src.node?.let { coreNode ->
                    factory.serializer(CoreNode::class.java, String::class.java)
                        .serialize(requestUUID, coreNode)
                }?.also { addProperty(SerializationKeys.KEY_NODE, it) }
            }
        }catch (e: Exception){
            logger.error("$requestUUID:${LOG_TAG}$TAG_SERIALIZE${e.safeMessage()}")
            null
        }
    }
}