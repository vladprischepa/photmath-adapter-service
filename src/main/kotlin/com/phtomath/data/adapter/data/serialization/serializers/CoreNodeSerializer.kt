package com.phtomath.data.adapter.data.serialization.serializers

import com.google.gson.GsonBuilder
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.phtomath.data.adapter.data.model.CoreNode
import com.phtomath.data.adapter.data.model.NodeType
import com.phtomath.data.adapter.data.serialization.CoreNodeJsonSerializer
import com.phtomath.data.adapter.data.serialization.base.ISerializationProvider
import com.phtomath.data.adapter.data.serialization.base.Serializer
import com.phtomath.data.adapter.data.serialization.deserializers.SerializationKeys
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class CoreNodeSerializer(
   logger: Logger,
   analytics: Analytics,
    factory: ISerializationProvider
): Serializer<CoreNode, JsonElement?>(logger, analytics, factory) {
    companion object{
        private const val SUB_TAG_SERIALIZE_NODE = "serializeNode:"
        private const val SUB_TAG_SERIALIZE_TYPE = "serializeType:"
    }

    private val gson = GsonBuilder()
        .registerTypeAdapter(CoreNode::class.java, CoreNodeJsonSerializer)
        .create()

    override fun serialize(requestUUID: String?, src: CoreNode): JsonElement? {
        return try {
            gson.toJsonTree(src, CoreNode::class.java)
        }catch (e: Exception){
            logger.error("$requestUUID:${LOG_TAG}$TAG_SERIALIZE", e.safeMessage())
            null
        }
    }

    private fun serializeNode(requestUUID: String?, src: CoreNode): JsonElement?{
        return try {
            JsonObject().apply {
                src.children?.mapNotNull { childNode -> serializeNode(requestUUID, childNode) }?.also { list ->
                    JsonArray().apply {
                        list.forEach { add(it) }
                    }
                }
                serializeType(requestUUID, src.type)?.also {
                    addProperty(SerializationKeys.KEY_TYPE, it)
                }
                src.color?.also {
                    addProperty(SerializationKeys.KEY_COLOR, it)
                }
                src.value?.also {
                    addProperty(SerializationKeys.KEY_VALUE, it)
                }
            }
        }catch (e: Exception){
            logger.error("$requestUUID:${LOG_TAG}$SUB_TAG_SERIALIZE_NODE", e.safeMessage())
            null
        }
    }

    private fun serializeType(requestUUID: String?, src: NodeType): String?{
        return try {
            src.key
        }catch (e: Exception){
            logger.error("$requestUUID:${LOG_TAG}$SUB_TAG_SERIALIZE_TYPE", e.safeMessage())
            null
        }
    }
}