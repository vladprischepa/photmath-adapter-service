package com.phtomath.data.adapter.data.serialization.serializers

import com.phtomath.data.adapter.data.latex.NodeToLatexConverter
import com.phtomath.data.adapter.data.model.CoreNode
import com.phtomath.data.adapter.data.serialization.base.ISerializationProvider
import com.phtomath.data.adapter.data.serialization.base.Serializer
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class CoreNodeToStringSerializer(
   logger: Logger,
   analytics: Analytics,
   factory: ISerializationProvider
) : Serializer<CoreNode, String?>(logger, analytics, factory) {
    override fun serialize(requestUUID: String?, src: CoreNode): String? {
        return  try {
            NodeToLatexConverter.nodeToLatexString(src)
        }catch (e: Exception){
            logger.error("$requestUUID:${LOG_TAG}$TAG_SERIALIZE", e.safeMessage())
            null
        }

    }
}