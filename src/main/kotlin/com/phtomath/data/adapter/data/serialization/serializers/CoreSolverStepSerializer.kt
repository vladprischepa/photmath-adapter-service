package com.phtomath.data.adapter.data.serialization.serializers

import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.phtomath.data.adapter.data.model.CoreSolverStep
import com.phtomath.data.adapter.data.model.CoreSolverSubstep
import com.phtomath.data.adapter.data.model.TextNode
import com.phtomath.data.adapter.data.serialization.base.ISerializationProvider
import com.phtomath.data.adapter.data.serialization.base.Serializer
import com.phtomath.data.adapter.data.serialization.deserializers.SerializationKeys
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class CoreSolverStepSerializer(
    logger: Logger,
    analytics: Analytics,
    factory: ISerializationProvider
): Serializer<CoreSolverStep, JsonElement?>(logger, analytics, factory) {
    override fun serialize(requestUUID: String?, src: CoreSolverStep): JsonElement? {
        return try {
            val textNodeToStringSerializer = factory.serializer(TextNode::class.java, String::class.java)
            val coreSolverSubstepSerializer = factory.serializer(CoreSolverSubstep::class.java, JsonElement::class.java)
            JsonObject().apply {
                src.headers?.mapNotNull { textNodeToStringSerializer.serialize(requestUUID, it) }?.also { list ->
                    val array = JsonArray()
                    list.forEach { array.add(it) }
                    add(SerializationKeys.KEY_HEADERS, array)
                }
                src.substeps?.mapNotNull { coreSolverSubstepSerializer.serialize(requestUUID, it) }?.also { list ->
                    val array = JsonArray()
                    list.forEach { array.add(it) }
                    add(SerializationKeys.KEY_SUBSTEPS, array)
                }
            }
        }catch (e: Exception){
            logger.error("$requestUUID:${LOG_TAG}$TAG_SERIALIZE", e.safeMessage())
            null
        }
    }
}