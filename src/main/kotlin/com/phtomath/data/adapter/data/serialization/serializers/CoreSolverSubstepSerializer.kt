package com.phtomath.data.adapter.data.serialization.serializers

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.phtomath.data.adapter.data.model.CoreNode
import com.phtomath.data.adapter.data.model.CoreSolverSubstep
import com.phtomath.data.adapter.data.model.StepsResult
import com.phtomath.data.adapter.data.model.TextNode
import com.phtomath.data.adapter.data.serialization.base.ISerializationProvider
import com.phtomath.data.adapter.data.serialization.base.Serializer
import com.phtomath.data.adapter.data.serialization.deserializers.SerializationKeys
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class CoreSolverSubstepSerializer(
    logger: Logger,
    analytics: Analytics,
    factory: ISerializationProvider
): Serializer<CoreSolverSubstep, JsonElement?>(logger, analytics, factory) {
    override fun serialize(requestUUID: String?, src: CoreSolverSubstep): JsonElement? {
        return try {
            val textToStringSerializer = factory.serializer(TextNode::class.java, String::class.java)
            val coreToStringSerializer = factory.serializer(CoreNode::class.java, String::class.java)
            val stepsResultSerializer = factory.serializer(StepsResult::class.java, JsonElement::class.java)
            JsonObject().apply {
                src.description?.let { textToStringSerializer.serialize(requestUUID, it) }?.also {
                    addProperty(SerializationKeys.KEY_DESCRIPTION, it)
                }
                src.left?.let { coreToStringSerializer.serialize(requestUUID, it) }?.also {
                    addProperty(SerializationKeys.KEY_LEFT, it)
                }
                src.right?.let { coreToStringSerializer.serialize(requestUUID, it) }?.also {
                    addProperty(SerializationKeys.KEY_RIGHT, it)
                }
                src.subresult?.let { stepsResultSerializer.serialize(requestUUID, it) }?.also {
                    add(SerializationKeys.KEY_SUBRESULT, it)
                }

            }
        }catch (e: Exception){
            logger.error("$requestUUID:${LOG_TAG}$TAG_SERIALIZE", e.safeMessage())
            null
        }
    }

}