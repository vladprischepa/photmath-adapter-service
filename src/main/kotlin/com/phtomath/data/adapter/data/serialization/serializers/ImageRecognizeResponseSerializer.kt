package com.phtomath.data.adapter.data.serialization.serializers

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.phtomath.data.adapter.data.model.ImageRecognizeResponse
import com.phtomath.data.adapter.data.model.Info
import com.phtomath.data.adapter.data.model.Result
import com.phtomath.data.adapter.data.serialization.base.ISerializationProvider
import com.phtomath.data.adapter.data.serialization.base.Serializer
import com.phtomath.data.adapter.data.serialization.deserializers.SerializationKeys
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class ImageRecognizeResponseSerializer(
    logger: Logger,
    analytics: Analytics,
    factory: ISerializationProvider
): Serializer<ImageRecognizeResponse, JsonElement?>(logger, analytics, factory) {
    override fun serialize(requestUUID: String?, src: ImageRecognizeResponse): JsonElement? {
        return try {
            val resultSerializer = factory.serializer(Result::class.java, JsonElement::class.java)
            val infoSerializer = factory.serializer(Info::class.java, JsonElement::class.java)
            JsonObject().apply {
                src.result?.let { resultSerializer.serialize(requestUUID, it) }?.also {
                    add(SerializationKeys.KEY_RESULT, it)
                }
                src.info?.let { infoSerializer.serialize(requestUUID, it) }?.also {
                    add(SerializationKeys.KEY_INFO, it)
                }
            }
        }catch (e: Exception){
            logger.error("$requestUUID:${LOG_TAG}$TAG_SERIALIZE${e.safeMessage()}")
            null
        }
    }
}