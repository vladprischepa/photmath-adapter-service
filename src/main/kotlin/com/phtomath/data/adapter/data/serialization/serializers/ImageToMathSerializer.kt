package com.phtomath.data.adapter.data.serialization.serializers

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.phtomath.data.adapter.data.model.CommandNode
import com.phtomath.data.adapter.data.model.ImageToMathInfo
import com.phtomath.data.adapter.data.serialization.base.ISerializationProvider
import com.phtomath.data.adapter.data.serialization.base.Serializer
import com.phtomath.data.adapter.data.serialization.deserializers.SerializationKeys
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class ImageToMathSerializer(
    logger: Logger,
    analytics: Analytics,
    factory: ISerializationProvider
): Serializer<ImageToMathInfo, JsonElement?>(logger, analytics, factory) {

    override fun serialize(requestUUID: String?, src: ImageToMathInfo): JsonElement? {
        return try {
            JsonObject().apply {
                src.version?.also {
                    addProperty(SerializationKeys.KEY_VERSION, it)
                }
                src.contentType?.also {
                    addProperty(SerializationKeys.KEY_CONTENT_TYPE, it)
                }
                src.fineGrainedContentType?.also {
                    addProperty(SerializationKeys.KEY_GRAINED_CONTENT_TYPE, it)
                }
                src.command?.let { commandNode ->
                    factory.serializer(CommandNode::class.java, JsonElement::class.java)
                        .serialize(requestUUID, commandNode)
                }?.also {
                    add(SerializationKeys.KEY_COMMAND, it)
                }
            }
        }catch (e: Exception){
            logger.error("$requestUUID:$LOG_TAG$TAG_SERIALIZE${e.safeMessage()}")
            null
        }
    }
}