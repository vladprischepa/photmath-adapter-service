package com.phtomath.data.adapter.data.serialization.serializers

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.phtomath.data.adapter.data.model.ImageToMathInfo
import com.phtomath.data.adapter.data.model.Info
import com.phtomath.data.adapter.data.model.SolverInfo
import com.phtomath.data.adapter.data.model.WritingTypeClassifier
import com.phtomath.data.adapter.data.serialization.base.DefaultSerializer
import com.phtomath.data.adapter.data.serialization.base.ISerializationProvider
import com.phtomath.data.adapter.data.serialization.base.Serializer
import com.phtomath.data.adapter.data.serialization.deserializers.SerializationKeys
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class InfoSerializer(
    logger: Logger,
    analytics: Analytics,
    factory: ISerializationProvider
): Serializer<Info, JsonElement?>(logger, analytics, factory) {

    override fun serialize(requestUUID: String?, src: Info): JsonElement? {
        return try {
            JsonObject().apply {
                src.solver?.let { solverInfo ->
                    factory.serializer(SolverInfo::class.java, JsonElement::class.java)
                        .serialize(requestUUID, solverInfo)
                }?.also { add(SerializationKeys.KEY_SOLVER, it) }
                src.imageToMath?.let { imageToMathInfo ->
                    factory.serializer(ImageToMathInfo::class.java, JsonElement::class.java)
                        .serialize(requestUUID, imageToMathInfo)
                }?.also { add(SerializationKeys.KEY_IMAGE_TO_MATH, it) }
                src.writingTypeClassifier?.let { writingTypeClassifier ->
                    DefaultSerializer.serialize(logger, requestUUID, writingTypeClassifier, WritingTypeClassifier::class.java)
//                    factory.serializer(WritingTypeClassifier::class.java, JsonElement::class.java)
//                        .serialize(requestUUID, writingTypeClassifier)
                }?.also { add(SerializationKeys.KEY_WRITING_CLASSIFIER, it) }
            }
        }catch (e: Exception){
            logger.error("$requestUUID:$LOG_TAG$TAG_SERIALIZE${e.safeMessage()}")
            null
        }
    }

}