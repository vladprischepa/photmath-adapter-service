package com.phtomath.data.adapter.data.serialization.serializers

import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.phtomath.data.adapter.data.model.Action
import com.phtomath.data.adapter.data.model.CoreNode
import com.phtomath.data.adapter.data.model.NodeAction
import com.phtomath.data.adapter.data.serialization.base.ISerializationProvider
import com.phtomath.data.adapter.data.serialization.base.Serializer
import com.phtomath.data.adapter.data.serialization.deserializers.SerializationKeys
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class NodeActionToStringSerializer(
    logger: Logger,
    analytics: Analytics,
    factory: ISerializationProvider
): Serializer<NodeAction, String?>(logger, analytics, factory) {

    override fun serialize(requestUUID: String?, src: NodeAction): String? {
        return try {
            val obj = JsonObject()
            src.action?.let { action ->
                factory.serializer(Action::class.java, JsonElement::class.java)
                    .serialize(requestUUID, action)
            }?.also { obj.add(SerializationKeys.KEY_ACTION, it) }
            src.node?.let { node ->
                factory.serializer(CoreNode::class.java, JsonElement::class.java)
                    .serialize(requestUUID, node)
            }?.also { obj.add(SerializationKeys.KEY_NODE, it) }
            obj.add(SerializationKeys.KEY_EXPERIMENTS, JsonObject())
            Gson().toJson(obj)
        }catch (e: Exception){
            logger.error("$requestUUID:${LOG_TAG}$TAG_SERIALIZE${e.safeMessage()}")
            null
        }
    }
}