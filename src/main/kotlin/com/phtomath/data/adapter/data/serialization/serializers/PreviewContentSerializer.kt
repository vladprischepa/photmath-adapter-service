package com.phtomath.data.adapter.data.serialization.serializers

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.phtomath.data.adapter.data.model.CoreNode
import com.phtomath.data.adapter.data.model.PreviewContent
import com.phtomath.data.adapter.data.model.TextNode
import com.phtomath.data.adapter.data.serialization.base.ISerializationProvider
import com.phtomath.data.adapter.data.serialization.base.Serializer
import com.phtomath.data.adapter.data.serialization.deserializers.SerializationKeys
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class PreviewContentSerializer(
    logger: Logger,
    analytics: Analytics,
    factory: ISerializationProvider
): Serializer<PreviewContent, JsonElement?>(logger, analytics, factory) {
    override fun serialize(requestUUID: String?, src: PreviewContent): JsonElement? {
        return try {
            val textNodeToStringSerializer = factory.serializer(TextNode::class.java, String::class.java)
            val coreNodeToStringSerializer = factory.serializer(CoreNode::class.java, String::class.java)
            JsonObject().apply {
                src.description?.let { textNodeToStringSerializer.serialize(requestUUID, it) }?.also {
                    addProperty(SerializationKeys.KEY_DESCRIPTION, it)
                }
                src.problem?.let { coreNodeToStringSerializer.serialize(requestUUID, it) }?.also {
                    addProperty(SerializationKeys.KEY_PROBLEM, it)
                }
                src.solution?.let { coreNodeToStringSerializer.serialize(requestUUID, it) }?.also {
                    addProperty(SerializationKeys.KEY_SOLUTION, it)
                }
            }
        }catch (e: Exception){
            logger.error("$requestUUID:${LOG_TAG}${Serializer.TAG_SERIALIZE}${e.safeMessage()}")
            null
        }
    }
}