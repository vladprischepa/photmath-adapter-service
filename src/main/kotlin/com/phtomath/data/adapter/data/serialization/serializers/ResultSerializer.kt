package com.phtomath.data.adapter.data.serialization.serializers

import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.phtomath.data.adapter.data.model.Result
import com.phtomath.data.adapter.data.model.VerticalCoreGroup
import com.phtomath.data.adapter.data.serialization.base.ISerializationProvider
import com.phtomath.data.adapter.data.serialization.base.Serializer
import com.phtomath.data.adapter.data.serialization.deserializers.SerializationKeys
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class ResultSerializer(
    logger: Logger,
    analytics: Analytics,
    factory: ISerializationProvider
): Serializer<Result, JsonElement?>(logger, analytics, factory) {
    override fun serialize(requestUUID: String?, src: Result): JsonElement? {
        return try {
            val verticalCoreGroupSerializer = factory.serializer(VerticalCoreGroup::class.java, JsonElement::class.java)
            JsonObject().apply {
                val array = JsonArray()
                src.groups.mapNotNull { verticalCoreGroupSerializer.serialize(requestUUID, it) }.forEach {
                    array.add(it)
                }
                add(SerializationKeys.KEY_SOLUTIONS, array)
            }
        }catch (e: Exception){
            logger.error("$requestUUID:${LOG_TAG}$TAG_SERIALIZE${e.safeMessage()}")
            null
        }
    }

}