package com.phtomath.data.adapter.data.serialization.serializers

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.phtomath.data.adapter.data.model.CommandNode
import com.phtomath.data.adapter.data.model.SolverInfo
import com.phtomath.data.adapter.data.serialization.base.ISerializationProvider
import com.phtomath.data.adapter.data.serialization.base.Serializer
import com.phtomath.data.adapter.data.serialization.deserializers.SerializationKeys
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class SolverInfoSerializer(
    logger: Logger,
    analytics: Analytics,
    factory: ISerializationProvider
): Serializer<SolverInfo, JsonElement?>(logger, analytics, factory) {

    override fun serialize(requestUUID: String?, src: SolverInfo): JsonElement? {
        return try {
            JsonObject().apply {
                src.errorType?.also{ addProperty(SerializationKeys.KEY_ERROR_TYPE, it) }
                src.ioVersion?.also { addProperty(SerializationKeys.KEY_IO_VERSION, it) }
                src.normalizedInput?.let { commandNode ->
                    factory.serializer(CommandNode::class.java, JsonElement::class.java)
                        .serialize(requestUUID, commandNode)
                }?.also { add(SerializationKeys.KEY_NORMALIZED_INPUT, it) }
                src.version?.also { addProperty(SerializationKeys.KEY_VERSION, it) }
            }
        }catch (e: Exception){
            logger.error("$requestUUID:$LOG_TAG$TAG_SERIALIZE${e.safeMessage()}")
            null
        }
    }

}