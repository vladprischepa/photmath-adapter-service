@file:Suppress("DuplicatedCode")

package com.phtomath.data.adapter.data.serialization.serializers

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.phtomath.data.adapter.data.model.Info
import com.phtomath.data.adapter.data.model.StepsResponse
import com.phtomath.data.adapter.data.model.StepsResult
import com.phtomath.data.adapter.data.serialization.base.ISerializationProvider
import com.phtomath.data.adapter.data.serialization.base.Serializer
import com.phtomath.data.adapter.data.serialization.deserializers.SerializationKeys
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class StepsResponseSerializer(
    logger: Logger,
    analytics: Analytics,
    factory: ISerializationProvider
): Serializer<StepsResponse, JsonElement?>(logger, analytics, factory) {
    override fun serialize(requestUUID: String?, src: StepsResponse): JsonElement? {
        return try {
            val stepsResultSerializer = factory.serializer(StepsResult::class.java, JsonElement::class.java)
            val infoSerializer = factory.serializer(Info::class.java, JsonElement::class.java)
            JsonObject().apply {
                src.result?.let { stepsResultSerializer.serialize(requestUUID, it) }?.also {
                    add(SerializationKeys.KEY_RESULT, it)
                }
                src.info?.let { infoSerializer.serialize(requestUUID, it) }?.also {
                    add(SerializationKeys.KEY_INFO, it)
                }
            }
        }catch (e: Exception){
            logger.error("$requestUUID:${LOG_TAG}$TAG_SERIALIZE", e.safeMessage())
            null
        }
    }
}