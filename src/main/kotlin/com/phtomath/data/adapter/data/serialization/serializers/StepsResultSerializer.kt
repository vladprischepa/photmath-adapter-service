package com.phtomath.data.adapter.data.serialization.serializers

import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.phtomath.data.adapter.data.model.CoreNode
import com.phtomath.data.adapter.data.model.CoreSolverStep
import com.phtomath.data.adapter.data.model.StepsResult
import com.phtomath.data.adapter.data.model.TextNode
import com.phtomath.data.adapter.data.serialization.base.ISerializationProvider
import com.phtomath.data.adapter.data.serialization.base.Serializer
import com.phtomath.data.adapter.data.serialization.deserializers.SerializationKeys
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class StepsResultSerializer(
    logger: Logger,
    analytics: Analytics,
    factory: ISerializationProvider
): Serializer<StepsResult, JsonElement?>(logger, analytics, factory){
    override fun serialize(requestUUID: String?, src: StepsResult): JsonElement? {
        return try {
            val textNodeToStringSerializer = factory.serializer(TextNode::class.java, String::class.java)
            val coreNodeToStringSerializer = factory.serializer(CoreNode::class.java, String::class.java)
            val coreSolverStepSerializer = factory.serializer(CoreSolverStep::class.java, JsonElement::class.java)
            JsonObject().apply {
                src.header?.let { textNodeToStringSerializer.serialize(requestUUID, it) }?.also {
                    addProperty(SerializationKeys.KEY_HEADER, it)
                }
                src.solution?.let { coreNodeToStringSerializer.serialize(requestUUID, it) }?.also {
                    addProperty(SerializationKeys.KEY_SOLUTION, it)
                }
                src.steps?.mapNotNull { coreSolverStepSerializer.serialize(requestUUID, it) }?.also { list ->
                    val array = JsonArray()
                    list.forEach { array.add(it) }
                    add(SerializationKeys.KEY_STEPS, array)
                }
            }
        }catch (e: Exception){
            e.printStackTrace()
            logger.error("$requestUUID:${LOG_TAG}$TAG_SERIALIZE", e.safeMessage())
            null
        }
    }
}