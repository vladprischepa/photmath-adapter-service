package com.phtomath.data.adapter.data.serialization.serializers

import com.phtomath.data.adapter.data.latex.TextNodeConverter
import com.phtomath.data.adapter.data.model.TextNode
import com.phtomath.data.adapter.data.serialization.base.ISerializationProvider
import com.phtomath.data.adapter.data.serialization.base.Serializer
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class TextNodeToStringSerializer(
    logger: Logger,
    analytics: Analytics,
    factory: ISerializationProvider
) : Serializer<TextNode, String?>(logger, analytics, factory){
    override fun serialize(requestUUID: String?, src: TextNode): String? {
        return try {
            TextNodeConverter.textNodeToString(src)
        }catch (e: Exception){
            logger.error("$requestUUID:${LOG_TAG}$TAG_SERIALIZE${e.safeMessage()}")
            null
        }
    }
}