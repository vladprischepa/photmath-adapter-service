package com.phtomath.data.adapter.data.serialization.serializers

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.phtomath.data.adapter.data.model.Translation
import com.phtomath.data.adapter.data.serialization.base.ISerializationProvider
import com.phtomath.data.adapter.data.serialization.base.Serializer
import com.phtomath.data.adapter.data.serialization.deserializers.SerializationKeys
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class TranslationSerializer(
    logger: Logger,
    analytics: Analytics,
    factory: ISerializationProvider
): Serializer<Translation, JsonElement?>(logger, analytics, factory) {

    override fun serialize(requestUUID: String?, src: Translation): JsonElement? {
        return try {
            JsonObject().apply {
                src.locale?.also { addProperty(SerializationKeys.KEY_LOCALE, it) }
                src.text?.also { addProperty(SerializationKeys.KEY_TEXT, it) }
            }
        }catch (e: Exception){
            logger.error("$requestUUID:${LOG_TAG}$TAG_SERIALIZE${e.safeMessage()}")
            null
        }
    }
}