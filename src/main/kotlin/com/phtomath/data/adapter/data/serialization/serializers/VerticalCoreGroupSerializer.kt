package com.phtomath.data.adapter.data.serialization.serializers

import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.phtomath.data.adapter.data.model.VerticalCoreGroup
import com.phtomath.data.adapter.data.model.VerticalEntry
import com.phtomath.data.adapter.data.serialization.base.ISerializationProvider
import com.phtomath.data.adapter.data.serialization.base.Serializer
import com.phtomath.data.adapter.data.serialization.deserializers.SerializationKeys
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeArg
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class VerticalCoreGroupSerializer(
    logger: Logger,
    analytics: Analytics,
    factory: ISerializationProvider
) : Serializer<VerticalCoreGroup, JsonElement?>(logger, analytics, factory) {
    override fun serialize(requestUUID: String?, src: VerticalCoreGroup): JsonElement? {
        return try {
            val verticalEntrySerializer = factory.serializer(VerticalEntry::class.java, JsonElement::class.java)
            JsonObject().apply {
                src.entries.mapNotNull { verticalEntrySerializer.serialize(requestUUID, it) }
                    .safeArg(emptyList()).also { list ->
                        val array = JsonArray().apply {
                            list.forEach { add(it) }
                        }
                        add(SerializationKeys.KEY_VARIANTS, array)
                    }
            }
        } catch (e: Exception) {
            logger.error("$requestUUID:${LOG_TAG}$TAG_SERIALIZE${e.safeMessage()}")
            null
        }
    }
}