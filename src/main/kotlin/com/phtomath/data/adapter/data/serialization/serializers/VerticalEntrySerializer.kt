package com.phtomath.data.adapter.data.serialization.serializers

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.phtomath.data.adapter.data.model.NodeAction
import com.phtomath.data.adapter.data.model.VerticalEntry
import com.phtomath.data.adapter.data.model.VerticalPreview
import com.phtomath.data.adapter.data.serialization.base.ISerializationProvider
import com.phtomath.data.adapter.data.serialization.base.Serializer
import com.phtomath.data.adapter.data.serialization.deserializers.SerializationKeys
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class VerticalEntrySerializer(
    logger: Logger,
    analytics: Analytics,
    factory: ISerializationProvider
): Serializer<VerticalEntry, JsonElement?>(logger, analytics, factory) {
    override fun serialize(requestUUID: String?, src: VerticalEntry): JsonElement? {
        return try {
            val nodeActionSerializer = factory.serializer(NodeAction::class.java, String::class.java)
            val previewSerializer = factory.serializer(VerticalPreview::class.java, JsonElement::class.java)
            JsonObject().apply {
                src.nodeAction?.let { nodeActionSerializer.serialize(requestUUID, it) }?.also {
                    addProperty(SerializationKeys.KEY_STEPS_REQUEST_JSON, it)
                }
                src.preview?.let { previewSerializer.serialize(requestUUID, it) }?.also {
                    add(SerializationKeys.KEY_PREVIEW, it)
                }
            }
        }catch (e: Exception){
            logger.error("$requestUUID:${LOG_TAG}$TAG_SERIALIZE${e.safeMessage()}")
            null
        }
    }
}