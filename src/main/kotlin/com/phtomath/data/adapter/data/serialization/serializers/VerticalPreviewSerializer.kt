package com.phtomath.data.adapter.data.serialization.serializers

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.phtomath.data.adapter.data.model.PreviewContent
import com.phtomath.data.adapter.data.model.TextNode
import com.phtomath.data.adapter.data.model.VerticalPreview
import com.phtomath.data.adapter.data.serialization.base.ISerializationProvider
import com.phtomath.data.adapter.data.serialization.base.Serializer
import com.phtomath.data.adapter.data.serialization.deserializers.SerializationKeys
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class VerticalPreviewSerializer(
    logger: Logger,
    analytics: Analytics,
    factory: ISerializationProvider
): Serializer<VerticalPreview, JsonElement?>(logger, analytics, factory) {
    override fun serialize(requestUUID: String?, src: VerticalPreview): JsonElement? {
        return try {
            val previewContentSerializer = factory.serializer(PreviewContent::class.java, JsonElement::class.java)
            val textNodeToStringSerializer = factory.serializer(TextNode::class.java, String::class.java)
            JsonObject().apply {
                src.content?.let { previewContentSerializer.serialize(requestUUID, it) }?.also {
                    add(SerializationKeys.KEY_CONTENT, it)
                }
                src.method?.let { textNodeToStringSerializer.serialize(requestUUID, it) }?.also {
                    addProperty(SerializationKeys.KEY_METHOD, it)
                }
                src.title?.let { textNodeToStringSerializer.serialize(requestUUID, it) }?.also {
                    addProperty(SerializationKeys.KEY_TITLE, it)
                }
            }
        }catch (e: Exception){
            logger.error("$requestUUID:${LOG_TAG}$TAG_SERIALIZE${e.safeMessage()}")
            null
        }
    }
}