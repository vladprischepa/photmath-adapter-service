package com.phtomath.data.adapter.data.serialization.serializers

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.phtomath.data.adapter.data.model.CommandNode
import com.phtomath.data.adapter.data.model.WritingTypeClassifier
import com.phtomath.data.adapter.data.serialization.base.ISerializationProvider
import com.phtomath.data.adapter.data.serialization.base.Serializer
import com.phtomath.data.adapter.data.serialization.deserializers.SerializationKeys
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger

class WritingTypeClassifierSerializer(
    logger: Logger,
    analytics: Analytics,
    factory: ISerializationProvider
): Serializer<WritingTypeClassifier, JsonElement?>(logger, analytics, factory){
    override fun serialize(requestUUID: String?, src: WritingTypeClassifier): JsonElement? {
        return  try {
            JsonObject().apply {
                src.version?.also {
                    addProperty(SerializationKeys.KEY_VERSION, it)
                }
                src.type?.also {
                    addProperty(SerializationKeys.KEY_TYPE, it)
                }
                src.score?.also {
                    addProperty(SerializationKeys.KEY_SCORE, it)
                }
            }
        }catch (e: Exception){
            logger.error("$requestUUID:$LOG_TAG$TAG_SERIALIZE${e.safeMessage()}")
            null
        }
    }
}