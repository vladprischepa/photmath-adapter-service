@file:Suppress("DuplicatedCode")

package com.phtomath.data.adapter.endpoints

import com.google.gson.Gson
import com.google.gson.JsonElement
import com.phtomath.data.adapter.data.model.ImageRecognizeResponse
import com.phtomath.data.adapter.data.model.RequestType
import com.phtomath.data.adapter.data.model.StepsResponse
import com.phtomath.data.adapter.data.serialization.*
import com.phtomath.data.adapter.newUUID
import com.phtomath.data.adapter.plugins.Analytics
import com.phtomath.data.adapter.safeArg
import com.phtomath.data.adapter.safeMessage
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.util.logging.*

private const val ENDPOINT_STEPS = "/steps"
private const val ENDPOINT_RECOGNIZE = "/recognize"

fun Route.imageRecognize(logger: Logger, typeFactory: TypeFactory): Route{
    return post(ENDPOINT_RECOGNIZE) {
        val requestUUID = newUUID
        try {
            val ticketId = call.request.headers["ticket_id"]
            logger.info("$requestUUID:ticketID: $ticketId")
            val imageUrl = call.request.headers["source"]
            logger.info("$requestUUID:imageUrl: $imageUrl")
            val responseJson = call.receive<JsonElement>()
            val deserializer = typeFactory.deserializer(ImageRecognizeResponse::class.java)
            val serializer = typeFactory.serializer(ImageRecognizeResponse::class.java, JsonElement::class.java)
            val imageRecognizeResponse = deserializer.deserialize(RequestType.RecognizeImage, requestUUID, ticketId, imageUrl, responseJson)
            val newBody = imageRecognizeResponse?.let { serializer.serialize(requestUUID, it) }?.let { Gson().toJson(it) }
            call.respondText(newBody.safeArg(), ContentType.Application.Json, HttpStatusCode.OK)
            logger.info("Recognize request ${requestUUID}, converted: SUCCESS!")

        }catch (e: Exception){
            e.printStackTrace()
            logger.error("Recognize request ${requestUUID}, FAIL: ${e.safeMessage()}")
            call.respond(HttpStatusCode.InternalServerError, e.safeMessage())
        }

    }
}

fun Route.steps(logger: Logger, typeFactory: TypeFactory): Route{
    return post(ENDPOINT_STEPS) {
        val requestUUID = newUUID
        try {
            val ticketId = call.request.headers["ticket_id"]
            logger.info("$requestUUID:ticketID: $ticketId")
            val imageUrl = call.request.headers["source"]
            logger.info("$requestUUID:imageUrl: $imageUrl")
            val responseJson = call.receive<JsonElement>()
            val deserializer = typeFactory.deserializer(StepsResponse::class.java)
            val serializer = typeFactory.serializer(StepsResponse::class.java, JsonElement::class.java)
            val stepsResponse = deserializer.deserialize(RequestType.StepsRequest, requestUUID, ticketId, imageUrl, responseJson)
            val newBody = stepsResponse?.let { serializer.serialize(requestUUID, it) }?.let { Gson().toJson(it) }
            call.respondText(newBody.safeArg(), ContentType.Application.Json, HttpStatusCode.OK)
            logger.info("Steps request ${requestUUID}, converted: SUCCESS!")

        }catch (e: Exception){
            e.printStackTrace()
            logger.error("Steps request ${requestUUID}, FAIL: ${e.safeMessage()}")
            call.respond(HttpStatusCode.InternalServerError, e.safeMessage())
        }

    }
}