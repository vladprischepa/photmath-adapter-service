package com.phtomath.data.adapter.plugins

import com.phtomath.data.adapter.data.model.NodeType
import com.phtomath.data.adapter.data.model.RequestType
import com.phtomath.data.adapter.safeArg
import io.ktor.client.*
import io.ktor.client.engine.okhttp.*
import io.ktor.client.request.*
import io.ktor.server.application.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class Analytics(
    private val application: Application
) {
    private val httpClient = HttpClient(OkHttp)
    private val scope: CoroutineScope = application
    private val logger = application.environment.log

    companion object{
        private const val STATS_REQUEST_URL = "https://t.rainide.com/765?metric=PhotomathUnrecognizedNodes&value=1"
        private const val K_NODE = "node"
        private const val K_NODE_JSON = "nodeJsonString"
        private const val K_VERSION = "version"
        private const val VALUE_VERSION = "1.0.5"
        private const val K_REQUEST_TYPE = "requestType"
        private const val K_REQUEST_UUID = "requestUUID"
        private const val K_TICKET_ID = "ticketID"
        private const val K_IMAGE_URL = "imageUrl"
    }


    fun sendUnrecognizedCoreNodeEvent(
        unknownNode: NodeType.Unknown,
        requestType: RequestType,
        requestUUID: String?,
        ticketID: String?,
        imageUrl: String?
    ){
        scope.launch {
            logger.error("Sending node data to clickhouse! Node: ${unknownNode.key}")
            val url = StringBuilder(STATS_REQUEST_URL).apply {
                append("&$K_NODE=${unknownNode.key}")
                append("&$K_NODE_JSON=${unknownNode.nodeJsonString}")
                append("&$K_VERSION=$VALUE_VERSION")
                append("&$K_REQUEST_TYPE=${requestType.name}")
                append("&$K_REQUEST_UUID=${requestUUID.safeArg()}")
                append("&$K_TICKET_ID=${ticketID.safeArg()}")
                append("&$K_IMAGE_URL=${imageUrl.safeArg()}")
            }.toString()
            httpClient.get(url)
        }
    }

    fun sendRecognizeRequestEvent(
        requestType: RequestType,
        requestUUID: String?,
        ticketID: String?,
        imageUrl: String?,

    ){
        scope.launch {

        }
    }
}

