@file:Suppress("DuplicatedCode")

package com.phtomath.data.adapter.plugins

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.phtomath.data.adapter.data.model.*
import com.phtomath.data.adapter.data.serialization.CoreNodeJsonSerializer
import com.phtomath.data.adapter.data.serialization.PreviewSerializer
import com.phtomath.data.adapter.data.serialization.ResultGroupsDeserializer
import com.phtomath.data.adapter.data.serialization.TypeFactory
import com.phtomath.data.adapter.endpoints.imageRecognize
import com.phtomath.data.adapter.endpoints.steps
import com.phtomath.data.adapter.safeMessage
import com.phtomath.data.adapter.sha256WithRsa
import io.ktor.http.*
import io.ktor.serialization.gson.*
import io.ktor.server.application.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.util.*
import kotlin.system.exitProcess

private const val ENDPOINT_HEALTH_CHECK = "/health"
private const val ENDPOINT_TERMINATE = "/kill"
private const val ENDPOINT_STEPS = "/steps"

fun Application.configureRouting() {

    val analytics = Analytics(this)
    CoreNodeJsonSerializer.injectLogger(environment.log, analytics)

    val typeFactory = TypeFactory(environment.log, analytics)
    val globalGson: Gson = GsonBuilder()
        .setPrettyPrinting()
        .setLenient()
        .disableHtmlEscaping()
        .registerTypeAdapter(CoreNode::class.java, CoreNodeJsonSerializer)
        .registerTypeAdapter(CoreGroup::class.java, ResultGroupsDeserializer(log))
        .registerTypeAdapter(VerticalPreview::class.java, PreviewSerializer())
        .create()

    install(ContentNegotiation) {
        register(ContentType.Application.Json, GsonConverter(globalGson))
    }


    routing {
        imageRecognize(application.log, typeFactory)
        steps(application.log, typeFactory)
        get(ENDPOINT_HEALTH_CHECK){
            try {
                call.respond(HttpStatusCode.OK)
                logger.info("Health Check: Status 200 OK!")
            }catch (e: Exception){
                logger.error("Steps request failed: ${e.message}")
                call.respond(HttpStatusCode.OK)
            }
        }
        get(ENDPOINT_TERMINATE){
            try {
                call.respond(HttpStatusCode.OK, "Terminating server...")
                logger.error("Terminating server...")
                exitProcess(1)
            }catch (e: Exception){
                logger.error("Exit failed: ${e.safeMessage()}")
            }
        }
        get("/sign"){
            try {
                val signToken = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCTBU1hPkT89ENa6rY7QG9ykkxI5ogXUIcPLzWq99A3YDnI/0CONhkvVfolUIJy6Z4dHulYCI8noOvuSiBs6L8Spv/S9vPGkU2zWDRQsRl2dr4zArefaxFO7D8ifK5oUyZYpWvnLZwcgE7wcEdKMbZFHQGUMRCsc2mImja0gUz0Q5j1LXupR0E48frYo4IuAy55Oxr4XzQICPW3ukDNq0AyoHhw0y0oMclHFhcRIDBhvlFpUoEwM4/9gG0yDPGGAKXqDvdtTiNz2MwmYgpDJ4ud274we9QK7rzTI/rjxj4cLo+XNklD4GyDgQzXME7/Ug7p+bx9GHDXBt8x5V8EtP1nAgMBAAECggEAdUPoaWTSf/SQeMb3XSFTA05/fDIsYHRBlbHxG5IXyON+k7xMkGHECsaIBBVIR0HhdJbARhBlppUrOeB0JooqRUVgjbOd00b4c9EpwSbRCXVC1csYDRbIMmwrG2XZcqrHzjL7cuQlPXJ2BD96DUrDeDLfnITaKMLRbTyb37O2XqkGILqF7Zmvlc2zr1ZmSFzdWMD3NK3U83w2UOpoN9i2lsL8E0w1vYTDBnZlsLz3MssMsTh6G/A085C8gsUq3KwMqMnLDnSZgxFavZT9KPcurqfYsxDdQrMbREu7Qlv4n2KvS9CPBgBNzOhN1YB20XkZf8pkjCn9fILblni9jA8ZoQKBgQDmROmUzj6wFhVjaJzRyU/JmsirUsIb0OgGI0muG/LA5dC5JvhEkBoAUj05eeiJMP4AlTQEqbeRRHmcrGnLKIDmJgP9gyv/kPZe4Oi+MvGoPhH/2fJi+7y2Ms4/6nCDBiOhYof/9KJnFDQLdbgQRjL9HNdqAICiYwXZq2y5StupcQKBgQCjcvs3fXifZlnTGNzGO1yC4wKETkYawCk6YvBC6jZO9zKme2F6a14iCxbB+8vZAfNnFfadiBMPSCiXX0ce0HSP10mM9i/9dpHxyNyWa5V4x2YnYc7PrHe17jF2cJWpPkXmtxuwfUkjpAP6JfXZ4HtdjKVmB4ORPk/++xSyaoDoVwKBgQDTfTeLlGG/GIlMZCikBWFiqOblc30UTyrLeF2Tq9epk2nApCohzrgS03Wn9G++kWC46FB+d+JBacLIQxJnpSu+New0SuV+NIYTXj5lciTBgUDG3EXi45LPRJpp2ci8jkbrUrPWQblnMwcZ/2SujcsG7hZrJUr/2jLlWKzjAf3+AQKBgD3Tgn9M8NGqz5NwNiKt1aKU6CDP0ujPg0jYn/VPztcz3m7nAMpjzqml9CfQbR1bBY0a27xe+oavTttjaO2ZAF5gmyu8/A4uSaphxj8H1vRmyJdhAe/AV0/0F7WrhJEOQzawUMMDwSTAJ9j5fvO5XDDYwL/FW9kW7v+OThfm/XflAoGBANnhbvdncca6o+vCOrO8XzZZ7uVWe+c//bxOaP5YTcJp4bQ8uUE84UTU1ZYYEbMeiL6LAERmXmByFEFiGy673O0kRsS1HJQvNywRdyz78iMm5qwWhdeIEkQUflbzhtjRvn8Oc0R9Y7JIYfDpeJXOzYpRdUTKTWg2zDmjH4FpttBV"
                val params = call.request.queryParameters.toMap().mapValues { it.value.firstOrNull() }
                val sign = buildString {
                    params.toSortedMap().forEach { (k, v) ->
                        append("$k=$v&")
                    }
                    deleteCharAt(this.lastIndexOf("&"))
                }.sha256WithRsa(signToken)
                call.respond(HttpStatusCode.OK, sign?:"null")
            }catch (e: Exception){
                logger.error("Failed to encrypt string")
                call.respond(HttpStatusCode.BadRequest)
            }
        }
    }
}
