package com.phtomath.data.adapter.plugins

import io.ktor.server.application.*
import io.ktor.util.logging.*
import io.ktor.util.pipeline.*

val PipelineContext<Unit, ApplicationCall>.logger: Logger get() = this.call.application.environment.log

val now: Long = System.currentTimeMillis()

@Suppress("UNCHECKED_CAST")
fun <R> Any.takeIfInstance(klass: Class<R>): R? {
    return if (klass.isInstance(this)) this as R else null
}