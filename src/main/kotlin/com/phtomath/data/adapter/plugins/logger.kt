package com.phtomath.data.adapter.plugins

import com.phtomath.data.adapter.safeArg
import com.phtomath.data.adapter.safeMessage
import io.ktor.util.logging.*

fun Logger.error(tag: String, message: String?)= error("$tag$message")

fun Logger.error(tag: String, error: Throwable) = error(tag, error.safeMessage())

fun Logger.info(tag: String, message: String?)= info("$tag$message")