package com.phtomath.data.adapter

import com.google.gson.GsonBuilder
import com.google.gson.JsonElement
import java.security.KeyFactory
import java.security.Signature
import java.security.spec.PKCS8EncodedKeySpec
import java.util.*

fun Throwable.safeMessage(): String{
    return this.message ?: this.javaClass.simpleName
}

fun String?.safeArg(defaultValue: String = ""): String{
    return if (this == null) defaultValue else requireNotNull(this)
}
fun Int?.safeArg(defaultValue: Int = 0): Int{
    return if (this == null) defaultValue else requireNotNull(this)
}

fun Long?.safeArg(defaultValue: Long = 0): Long{
    return if (this == null) defaultValue else requireNotNull(this)
}
fun Boolean?.safeArg(defaultValue: Boolean = false): Boolean{
    return if (this == null) defaultValue else requireNotNull(this)
}

fun Float?.safeArg(defaultValue: Float = 0f): Float{
    return if (this == null) defaultValue else requireNotNull(this)
}

fun <T> T?.safeArg(defaultValue: T): T{
    return if (this == null) defaultValue else requireNotNull(this)
}

val newUUID: String get() = UUID.randomUUID().toString()

fun JsonElement.toJsonString(prettyPrint: Boolean = false): String?{
    return try {
        val gson = GsonBuilder().apply {
            if (prettyPrint) setPrettyPrinting()
        }.create()
        gson.toJson(this)
    }catch (e: Exception){
        null
    }
}

fun String.sha256WithRsa(key:String): String?{
    return try{
        val pk = Base64.getDecoder().decode(key)
        val spec = PKCS8EncodedKeySpec(pk)
        val keyFactory = KeyFactory.getInstance("RSA")
        val signature = Signature.getInstance("SHA256withRSA")
        signature.initSign(keyFactory.generatePrivate(spec))
        signature.update(this.toByteArray())
        Base64.getEncoder().encodeToString(signature.sign())
    }catch (e: Exception){
        e.printStackTrace()
        null
    }
}